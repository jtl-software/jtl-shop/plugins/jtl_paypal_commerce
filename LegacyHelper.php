<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce;

use JTL\Cart\Cart;
use JTL\Checkout\Lieferadresse;
use JTL\Checkout\OrderHandler;
use JTL\Customer\Customer;
use JTL\Customer\Registration\Form as RegistrationForm;
use JTL\Helpers\Form;
use JTL\Helpers\ShippingMethod;
use JTL\Helpers\Tax;
use JTL\Router\Controller\CheckoutController;
use JTL\Router\Controller\OrderCompleteController;
use JTL\Session\Frontend;
use JTL\Shop;
use JTL\Shopsetting;
use stdClass;

use function Functional\first;
use function Functional\sort;

/**
 * Class LegacyHelper
 * @package Plugin\jtl_paypal_commerce
 */
class LegacyHelper
{
    private static ?CheckoutController $checkoutController = null;

    private static ?OrderCompleteController $orderCompleteController = null;

    /**
     * @return CheckoutController
     */
    private static function getCheckoutController(): CheckoutController
    {
        if (self::$checkoutController === null) {
            self::$checkoutController = new CheckoutController(
                Shop::Container()->getDB(),
                Shop::Container()->getCache(),
                Shop::getRouter()->getState(),
                Shopsetting::getInstance()->getAll(),
                AlertService::getInstance()
            );
            self::$checkoutController->init();
        }

        return self::$checkoutController;
    }

    private static function getOrderCompleteController(): OrderCompleteController
    {
        if (self::$orderCompleteController === null) {
            self::$orderCompleteController = new OrderCompleteController(
                Shop::Container()->getDB(),
                Shop::Container()->getCache(),
                Shop::getRouter()->getState(),
                Shopsetting::getInstance()->getAll(),
                AlertService::getInstance()
            );
            self::$orderCompleteController->init();
        }

        return self::$orderCompleteController;
    }

    /**
     * @return string
     */
    public static function baueBestellnummer(): string
    {
        $orderHandler = new OrderHandler(
            Shop::Container()->getDB(),
            Frontend::getCustomer(),
            Frontend::getCart()
        );

        return \mb_substr($orderHandler->createOrderNo(), 0, 20);
    }

    /**
     * @param int   $shippingMethod
     * @param array $formValues
     * @param bool  $bMsg
     * @return bool
     */
    public static function pruefeVersandartWahl(int $shippingMethod, array $formValues, bool $bMsg = true): bool
    {
        return self::getCheckoutController()->checkShippingSelection($shippingMethod, $formValues, $bMsg);
    }

    /**
     * @param array $data
     * @return int|null
     */
    public static function pruefeZahlungsartwahlStep(array $data): ?int
    {
        return self::getCheckoutController()->checkStepPaymentMethodSelection($data);
    }

    /**
     * @param array $missingData
     * @return int
     */
    public static function angabenKorrekt(array $missingData): int
    {
        return Form::hasNoMissingData($missingData);
    }

    /**
     * @param array $data
     * @return Lieferadresse
     */
    public static function getLieferdaten(array $data): Lieferadresse
    {
        return Lieferadresse::createFromPost($data);
    }

    /**
     * @param array $data
     * @return array
     */
    public static function checkLieferFormularArray(array $data): array
    {
        return (new RegistrationForm())->checkLieferFormularArray($data);
    }

    /**
     * @param array $data
     * @param int   $customerAccount
     * @param int   $htmlentities
     * @return Customer
     */
    public static function getKundendaten(array $data, int $customerAccount, int $htmlentities = 1): Customer
    {
        return (new RegistrationForm())->getCustomerData($data, (bool)$customerAccount, (bool)$htmlentities);
    }

    /**
     * @param array $data
     * @param int   $customerAccount
     * @param int   $checkpass
     * @return array
     */
    public static function checkKundenFormularArray(array $data, int $customerAccount, int $checkpass = 1): array
    {
        return (new RegistrationForm())->checkKundenFormularArray($data, (bool)$customerAccount, (bool)$checkpass);
    }

    /**
     * @param int $shippingMethodID
     * @param int $customerGroupID
     * @return array
     */
    public static function gibZahlungsarten(int $shippingMethodID, int $customerGroupID): array
    {
        return self::getCheckoutController()->getPaymentMethods($shippingMethodID, $customerGroupID);
    }

    /**
     * @param int    $methodId
     * @param int    $shippingId
     * @param int    $customerGroupId
     * @param string $countryCode
     * @param bool   $gross
     * @return float
     */
    public static function getPaymentSurchargeDiscount(
        int $methodId,
        int $shippingId,
        int $customerGroupId,
        string $countryCode,
        bool $gross
    ): float {
        $cart   = Frontend::getCart();
        $method = first(
            self::gibZahlungsarten($shippingId, $customerGroupId),
            function (stdClass $method) use ($methodId) {
                return (int)$method->kZahlungsart === $methodId;
            }
        );

        $surcharge = (float)$method->fAufpreis;
        if ($method->cAufpreisTyp === 'prozent') {
            $fGuthaben = $_SESSION['Bestellung']->fGuthabenGenutzt ?? 0;
            $surRec    = $cart->gibGesamtsummeWarenExt([
                \C_WARENKORBPOS_TYP_ARTIKEL,
                \C_WARENKORBPOS_TYP_VERSANDPOS,
                \C_WARENKORBPOS_TYP_KUPON,
                \C_WARENKORBPOS_TYP_GUTSCHEIN,
                \C_WARENKORBPOS_TYP_VERSANDZUSCHLAG,
                \C_WARENKORBPOS_TYP_NEUKUNDENKUPON,
                \C_WARENKORBPOS_TYP_VERSAND_ARTIKELABHAENGIG,
                \C_WARENKORBPOS_TYP_VERPACKUNG,
            ], true);
            $surcharge = (($surRec - $fGuthaben) * $method->fAufpreis) / 100.0;
        }
        if ($gross) {
            $taxRate   = Tax::getSalesTax(
                $cart->gibVersandkostenSteuerklasse($countryCode)
            );
            $surcharge = $surcharge / (100 + $taxRate) * 100.0;
        }

        return $surcharge;
    }

    public static function getCheapestShippingMethod(
        array $shippingMethods,
        int $paymentMethodId,
        int $customerGroupId
    ): ?object {
        if ($paymentMethodId > 0) {
            /** @var stdClass[] $shippingMethods */
            $shippingMethods = \array_filter(
                $shippingMethods,
                static function (stdClass $method) use ($paymentMethodId, $customerGroupId) {
                    $paymentMethods = ShippingMethod::getPaymentMethods(
                        (int)$method->kVersandart,
                        $customerGroupId,
                        $paymentMethodId
                    );

                    return !empty($paymentMethods);
                }
            );
        }

        return first(sort($shippingMethods, static function (object $a, object $b) {
            if ($a->fEndpreis === $b->fEndpreis) {
                return $a->nSort < $b->nSort ? -1 : 1;
            }

            return $a->fEndpreis < $b->fEndpreis ? -1 : 1;
        }));
    }

    public static function isOrderComplete(Cart $cart, &$errCode): bool
    {
        $controller = self::getOrderCompleteController();
        if ($controller->isOrderComplete($cart)) {
            $errCode = 0;

            return true;
        }

        $errCode = $controller->getErorCode();

        return false;
    }
}
