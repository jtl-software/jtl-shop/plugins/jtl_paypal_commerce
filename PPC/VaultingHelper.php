<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\PPC;

use Exception;
use GuzzleHttp\Exception\GuzzleException;
use JTL\Checkout\Adresse;
use JTL\Checkout\Bestellung;
use JTL\DB\DbInterface;
use JTL\Shop;
use Plugin\jtl_paypal_commerce\paymentmethod\PayPalPaymentInterface;
use Plugin\jtl_paypal_commerce\PPC\Authorization\Token;
use Plugin\jtl_paypal_commerce\PPC\HttpClient\PPCClient;
use Plugin\jtl_paypal_commerce\PPC\Order\Address;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\PaymentSourceBuilder;
use Plugin\jtl_paypal_commerce\PPC\Order\Shipping;
use Plugin\jtl_paypal_commerce\PPC\Order\Vaulting\PaymentTokenGetRequest;
use Plugin\jtl_paypal_commerce\PPC\Order\Vaulting\PaymentTokenResponse;
use Plugin\jtl_paypal_commerce\PPC\Request\Serializer\JSON;

/**
 * Class Vaulting
 * @package Plugin\jtl_paypal_commerce\PPC
 */
class VaultingHelper
{
    private DbInterface $db;

    private Bestellung $shopOrder;

    private Configuration $config;

    private static ?bool $vaultingEnabled = null;

    private const VAULTED_PAYMENTS = [
        PaymentSourceBuilder::FUNDING_PAYPAL,
    ];

    /**
     * VaultingHelper constructor
     */
    public function __construct(Configuration $config, ?Bestellung $shopOrder = null, ?DbInterface $db = null)
    {
        $this->db        = $db ?? Shop::Container()->getDB();
        $this->shopOrder = $shopOrder ?? new Bestellung();
        $this->config    = $config;
    }

    public static function buildShippingHashFromAdress(Address $address): string
    {
        try {
            $adrObject = \json_decode((string)$address, true, 512, JSON_THROW_ON_ERROR);
        } catch (\JsonException) {
            return '';
        }
        ksort($adrObject, SORT_STRING);

        try {
            return sha1(\json_encode($adrObject, JSON_THROW_ON_ERROR));
        } catch (\JsonException) {
            return '';
        }
    }

    public function buildShippingHash(): string
    {
        $shippingId = (int)$this->shopOrder->kLieferadresse;
        if ($shippingId === 0) {
            $shippingId      = (int)$this->shopOrder->kRechnungsadresse;
            $shippingAddress = $this->db->getSingleObject(
                'SELECT cPLZ, cOrt, cStrasse, cHausnummer, cLand
                    FROM trechnungsadresse WHERE kRechnungsadresse = :shippingId',
                ['shippingId' => $shippingId]
            );
        } else {
            $shippingAddress = $this->db->getSingleObject(
                'SELECT cPLZ, cOrt, cStrasse, cHausnummer, cLand
                    FROM tlieferadresse WHERE kLieferadresse = :shippingId',
                ['shippingId' => $shippingId]
            );
        }

        return self::buildShippingHashFromAdress(
            Address::createFromOrderAddress((new Adresse())->fromObject($shippingAddress)->decrypt())
        );
    }

    public function storeVault(string $source, JSON $vaultData): void
    {
        $vaultState    = $vaultData->getData()->status ?? '';
        $vaultId       = $vaultData->getData()->id ?? null;
        $vaultCustomer = $vaultData->getData()->customer ?? null;
        $vaultActive   = $vaultId !== null && $vaultCustomer !== null && ($vaultCustomer->id ?? null) !== null;
        if (
            (int)$this->shopOrder->kKunde === 0
            || !$vaultActive
            || !\in_array($vaultState, ['VAULTED', 'APPROVED'])
        ) {
            return;
        }

        $data = (object)[
            'customer_id'    => $this->shopOrder->kKunde,
            'payment_id'     => $this->shopOrder->kZahlungsart,
            'funding_source' => $source,
            'vault_id'       => $vaultId,
            'vault_status'   => $vaultState,
            'vault_customer' => $vaultCustomer->id,
            'shipping_hash'  => $this->buildShippingHash(),
        ];
        $this->db->upsert(
            'xplugin_jtl_paypal_checkout_vaulting',
            $data,
            ['customer_id', 'payment_id', 'funding_source', 'shipping_hash']
        );
    }

    public function deleteVault(string $vaultId): void
    {
        $this->db->delete('xplugin_jtl_paypal_checkout_vaulting', 'vault_id', $vaultId);
    }

    public function isVaultingEnabled(string $fundingSource, ?int $customerId = null): bool
    {
        if ($customerId === 0 || !\in_array($fundingSource, self::VAULTED_PAYMENTS, true)) {
            return false;
        }

        if (static::$vaultingEnabled === null) {
            static::$vaultingEnabled =
                ($this->config->getPrefixedConfigItem('vaultingDisplay_activateVaulting', 'N') === 'Y')
                && (int)$this->config->getPrefixedConfigItem('PaymentVaultingAvail', '0') > 0;
        }

        return static::$vaultingEnabled;
    }

    private function getVault(int $customerId, int $paymentId, string $fundingSource): ?object
    {
        return $this->db->getSingleObject(
            'SELECT vault_id, vault_customer, vault_status, shipping_hash
                FROM xplugin_jtl_paypal_checkout_vaulting
                WHERE customer_id = :cId
                    AND payment_id = :pId
                    AND funding_source = :fundingSource',
            [
                'cId' => $customerId,
                'pId' => $paymentId,
                'fundingSource' => $fundingSource,
            ]
        );
    }

    public function enableVaulting(string $fundingSource, PayPalPaymentInterface $paymentMethod, bool $enable): bool
    {
        $vaultingEnabled = $this->isVaultingEnabled($fundingSource);
        if ($vaultingEnabled && $enable) {
            $paymentMethod->addCache('ppc_vaulting_enable', 'Y');

            return true;
        }

        $paymentMethod->unsetCache('ppc_vaulting_enable');

        return false;
    }

    public function isVaultingActive(int $customerId, int $paymentId, string $fundingSource): bool
    {
        if ($customerId === 0) {
            return false;
        }

        return $this->getVault($customerId, $paymentId, $fundingSource) !== null;
    }

    public function getVaultCustomer(int $customerId, int $paymentId, string $fundingSource): ?string
    {
        if ($customerId === 0) {
            return null;
        }

        $vault = $this->getVault($customerId, $paymentId, $fundingSource);

        return $vault === null ? null : $vault->vault_customer;
    }

    public function isValidVaultCustomer(string $vaultCustomer, string $shippingHash): bool
    {
        $vault = $this->db->getSingleObject(
            'SELECT vault_id, shipping_hash
                FROM xplugin_jtl_paypal_checkout_vaulting
                WHERE vault_customer = :vaultCustomer
                    AND vault_status = \'VAULTED\'',
            [
                'vaultCustomer' => $vaultCustomer,
            ]
        );

        $vaultId = $vault === null ? null : $vault->vault_id;
        if ($vaultId === null) {
            return false;
        }

        return $vault->shipping_hash === $shippingHash;
    }

    public function getShippingAddress(int $customerId, int $paymentId, string $fundingSource): ?Address
    {
        if (!$this->isVaultingActive($customerId, $paymentId, $fundingSource)) {
            return null;
        }

        $vault   = $this->getVault($customerId, $paymentId, $fundingSource);
        $vaultId = $vault === null ? null : $vault->vault_id;
        if ($vaultId === null || $vault->vault_status !== 'VAULTED') {
            return null;
        }

        $client = new PPCClient(PPCHelper::getEnvironment());
        try {
            $paymentTokenResponse = new PaymentTokenResponse($client->send(
                new PaymentTokenGetRequest(Token::getInstance()->getToken(), $vaultId)
            ));
        } catch (GuzzleException | Exception) {
            return null;
        }

        $paymentSource = $paymentTokenResponse->getPaymentToken()->getPaymentSource($fundingSource);
        $shipping      = $paymentSource === null ? null : new Shipping($paymentSource->getProperty('shipping'));

        return $shipping === null ? null : $shipping->getAddress();
    }
}
