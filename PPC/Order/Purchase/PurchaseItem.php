<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\PPC\Order\Purchase;

use InvalidArgumentException;
use Plugin\jtl_paypal_commerce\PPC\Order\Amount;
use Plugin\jtl_paypal_commerce\PPC\PPCHelper;
use Plugin\jtl_paypal_commerce\PPC\Request\Serializer\JSON;
use Plugin\jtl_paypal_commerce\PPC\Request\Serializer\SerializerInterface;

/**
 * Class PurchaseItem
 * @package Plugin\jtl_paypal_commerce\PPC\Order
 */
class PurchaseItem extends JSON
{
    /** @var string    Goods that are stored, delivered, and used in their electronic format. */
    public const CATEGORY_DIGITAL = 'DIGITAL_GOODS';

    /** @var string    A tangible item that can be shipped with proof of delivery */
    public const CATEGORY_PHYSICAL = 'PHYSICAL_GOODS';

    /** @var string    A contribution or gift for which no good or service is exchanged. */
    public const CATEGORY_DONATION = 'DONATION';

    protected const CATGERY_ENUM = [
        self::CATEGORY_DIGITAL, self::CATEGORY_PHYSICAL, self::CATEGORY_DONATION
    ];

    /**
     * PurchaseItem constructor
     */
    public function __construct(?object $data = null)
    {
        parent::__construct($data ?? (object)[
            'name'        => '',
            'unit_amount' => new Amount(),
            'quantity'    => 0.0,
        ]);
    }

    /**
     * @inheritDoc
     */
    public function setData(string|array|object $data): static
    {
        parent::setData($data);

        $amountData = $this->getData()->unit_amount ?? null;
        if ($amountData !== null && !($amountData instanceof Amount)) {
            $this->setAmount(new Amount($amountData));
        }
        $taxData = $this->getData()->tax ?? null;
        if ($taxData !== null && !($taxData instanceof Amount)) {
            $this->setTax(new Amount($taxData));
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->data->name ?? '';
    }

    /**
     * @param string $name
     * @return self
     */
    public function setName(string $name): self
    {
        $this->data->name = PPCHelper::shortenStr($name, 127);

        return $this;
    }

    /**
     * @return Amount
     */
    public function getAmount(): Amount
    {
        return $this->data->unit_amount ?? new Amount();
    }

    /**
     * @param Amount $amount
     * @return self
     */
    public function setAmount(Amount $amount): self
    {
        $this->data->unit_amount = $amount;

        return $this;
    }

    /**
     * @return Amount
     */
    public function getTax(): Amount
    {
        return $this->data->tax ?? new Amount();
    }

    /**
     * @param Amount $tax
     * @return self
     */
    public function setTax(Amount $tax): self
    {
        $this->data->tax = $tax;

        return $this;
    }

    /**
     * @return float
     */
    public function getTaxrate(): float
    {
        return (float)($this->data->tax_rate ?? 0.0);
    }

    /**
     * @param float $taxRate
     * @return self
     */
    public function setTaxrate(float $taxRate): self
    {
        $this->data->tax_rate = $taxRate;

        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return (int)$this->data->quantity;
    }

    /**
     * @param int $quantity
     * @return self
     */
    public function setQuantity(int $quantity): self
    {
        $this->data->quantity = $quantity;

        return $this;
    }

    /**
     * @return string
     */
    public function getCategory(): string
    {
        return $this->data->category ?? self::CATEGORY_PHYSICAL;
    }

    /**
     * @param string $category
     * @return self
     */
    public function setCategory(string $category): self
    {
        if (!\in_array($category, self::CATGERY_ENUM)) {
            throw new InvalidArgumentException($category . ' is not a valid purchase category');
        }

        $this->data->category = PPCHelper::shortenStr($category, 20);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): mixed
    {
        $data = clone $this->getData();

        if (empty($data->tax) || ($data->tax instanceof SerializerInterface && $data->tax->isEmpty())) {
            unset($data->tax);
        }

        $data->quantity = \number_format($this->getQuantity(), 0, '.', '');
        $data->tax_rate = \number_format($this->getTaxrate(), 2, '.', '');

        return $data;
    }
}
