<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\PPC\Order\Payment\PaymentSourceProperties;

/**
 * Class PropertyConstants
 * @package Plugin\jtl_paypal_commerce\PPC\Order\Payment\PaymentSourceProperties
 */
class PropertyConstants
{
    public const TYPE_BILLING_AGREEMENT = 'BILLING_AGREEMENT';
}
