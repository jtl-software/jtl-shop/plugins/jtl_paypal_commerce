<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\PPC\Order;

use InvalidArgumentException;

/**
 * Class InvalidAmountException
 * @package Plugin\jtl_paypal_commerce\PPC\Order
 */
class InvalidAmountException extends InvalidArgumentException
{
}
