<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\PPC\Onboarding;

/**
 * Class Endpoint
 * @package Plugin\jtl_paypal_commerce\PPC\Onboarding
 */
class Endpoint
{
    public const PARTNER_CREDENTIALS  = '/merchant-integrations/credentials';
    public const PARTNER_MERCHANTINFO = '/merchant-integrations/';
}
