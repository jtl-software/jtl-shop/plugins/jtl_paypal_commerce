<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\PPC\Webhook;

use Exception;
use GuzzleHttp\Exception\GuzzleException;
use JsonException;
use JTL\Link\LinkInterface;
use JTL\Plugin\PluginInterface;
use JTL\Shop;
use Plugin\jtl_paypal_commerce\paymentmethod\Helper;
use Plugin\jtl_paypal_commerce\PPC\Authorization\AuthorizationException;
use Plugin\jtl_paypal_commerce\PPC\Authorization\Token;
use Plugin\jtl_paypal_commerce\PPC\Configuration;
use Plugin\jtl_paypal_commerce\PPC\HttpClient\PPCClient;
use Plugin\jtl_paypal_commerce\PPC\Logger;
use Plugin\jtl_paypal_commerce\PPC\Order\Capture;
use Plugin\jtl_paypal_commerce\PPC\PPCHelper;
use Plugin\jtl_paypal_commerce\PPC\Request\UnexpectedResponseException;
use Plugin\jtl_paypal_commerce\PPC\VaultingHelper;

use function Functional\first;

/**
 * Class Webhook
 * @package Plugin\jtl_paypal_commerce\PPC\Webhook
 */
class Webhook
{
    /** @var Configuration */
    protected Configuration $config;

    /** @var PluginInterface */
    protected PluginInterface $plugin;

    /** @var Logger */
    private Logger $logger;

    public const REGISTERED_EVENTS = [
        EventType::CAPTURE_COMPLETED,
        EventType::CAPTURE_DENIED,
        EventType::CAPTURE_REVERSED,
        EventType::VAULT_TOKEN_DELETED,
    ];

    /**
     * Webhook constructor.
     * @param PluginInterface $plugin
     * @param Configuration   $config
     */
    public function __construct(PluginInterface $plugin, Configuration $config)
    {
        $this->plugin = $plugin;
        $this->config = $config;
        $this->logger = new Logger(Logger::TYPE_INFORMATION);
    }

    /**
     * @param int         $exitCode
     * @param string|null $content
     * @noinspection PhpNoReturnAttributeCanBeAddedInspection
     */
    protected function exitResult(int $exitCode = 200, ?string $content = null): void
    {
        $headers = [
            200 => 'OK',
            400 => 'Bad Request',
            500 => 'Internal Server Error',
        ];
        if (!\array_key_exists($exitCode, $headers)) {
            $exitCode = 500;
        }
        \ob_end_clean();
        \header(\sprintf('%s %d %s', $_SERVER['SERVER_PROTOCOL'], $exitCode, $headers[$exitCode]));
        if ($content !== null) {
            echo $content;
        }

        exit();
    }

    /**
     * @param string $eventType
     * @param string $resourceType
     * @return void
     */
    protected function checkRegistration(string $eventType, string $resourceType): void
    {
        if (!\in_array($resourceType, ['capture', 'payment_token'], true)) {
            $this->logger->write(
                \LOGLEVEL_NOTICE,
                'Webhook::handleCall - resource type (' . $resourceType . ') not supported'
            );

            $this->exitResult();
            exit;
        }

        if (!\in_array($eventType, self::REGISTERED_EVENTS, true)) {
            $this->logger->write(
                \LOGLEVEL_NOTICE,
                'Webhook::handleCall - Event (' . $eventType . ') not registered'
            );

            $this->exitResult();
            exit;
        }
    }

    /**
     * @param string              $webhookId
     * @param WebhookCallResponse $response
     * @return void
     */
    protected function checkVerification(string $webhookId, WebhookCallResponse $response): void
    {
        $environment = PPCHelper::getEnvironment($this->config);
        $client      = new PPCClient($environment);
        try {
            if (
                !\defined('PAYPAL_WEBHOOK_NOT_VERIFY')
                || \PAYPAL_WEBHOOK_NOT_VERIFY !== true
                || !$environment->isSandbox()
            ) {
                $verifyRequest = new WebhookVerifySignatureRequest(
                    Token::getInstance()->getToken(),
                    $_SERVER['HTTP_PAYPAL_AUTH_ALGO'] ?? '',
                    $_SERVER['HTTP_PAYPAL_CERT_URL'] ?? '',
                    $_SERVER['HTTP_PAYPAL_TRANSMISSION_ID'] ?? '',
                    $_SERVER['HTTP_PAYPAL_TRANSMISSION_SIG'] ?? '',
                    $_SERVER['HTTP_PAYPAL_TRANSMISSION_TIME'] ?? '',
                    $webhookId,
                    $response->getOriginalData()
                );

                $verifyResponse = new WebhookVerifySignatureResponse($client->send($verifyRequest));
                if (!$verifyResponse->isVerified()) {
                    $this->logger->write(
                        \LOGLEVEL_ERROR,
                        'Webhook::handleCall - Webhook not verified:',
                        [
                            'response'  => $verifyResponse,
                            'webhookId' => $webhookId,
                            'content'   => $response->getOriginalData(),
                            'header'    => [
                                'PAYPAL_AUTH_ALGO'         => $_SERVER['HTTP_PAYPAL_AUTH_ALGO'] ?? 'NULL',
                                'PAYPAL_CERT_URL'          => $_SERVER['HTTP_PAYPAL_CERT_URL'] ?? 'NULL',
                                'PAYPAL_TRANSMISSION_ID'   => $_SERVER['HTTP_PAYPAL_TRANSMISSION_ID'] ?? 'NULL',
                                'PAYPAL_TRANSMISSION_SIG'  => $_SERVER['HTTP_PAYPAL_TRANSMISSION_SIG'] ?? 'NULL',
                                'PAYPAL_TRANSMISSION_TIME' => $_SERVER['HTTP_PAYPAL_TRANSMISSION_TIME'] ?? 'NULL',
                            ],
                        ]
                    );

                    $this->exitResult(400, 'Webhook not verified.');
                    exit();
                }

                $this->logger->write(\LOGLEVEL_DEBUG, 'Webhook::handleCall - Webhook verified:', $verifyResponse);
                return;
            }
        } catch (Exception | GuzzleException $e) {
            $this->logger->write(
                \LOGLEVEL_ERROR,
                'Webhook::handleCall - verification failed: ' . $e->getMessage()
            );

            $this->exitResult(400, 'Webhook verification failed.');
            exit();
        }

        $this->logger->write(\LOGLEVEL_DEBUG, 'Webhook::handleCall - no verification executed');
    }

    protected function checkDataValidation(bool|string $content, string $webhookId): void
    {
        if (empty($content)) {
            $this->exitResult(500, 'No data received');
            exit();
        }
        if (empty($webhookId)) {
            $this->exitResult(500, 'Webhook not configured');
            exit();
        }
    }

    /**
     * @return string|null
     */
    public function getWebHookURL(): ?string
    {
        /** @var LinkInterface $link */
        $link = $this->plugin->getLinks()->getLinks()->first(static function (LinkInterface $link) {
            return $link->getTemplate() === ('webhook_PayPalCommerce.tpl');
        });
        if ($link === null || $link->getSEO() === \ltrim($_SERVER['REQUEST_URI'], '/')) {
            return null;
        }

        return first($link->getURLs());
    }

    /**
     * @return object|null
     * @throws WebhookException
     */
    public function createWebhook(): ?object
    {
        $client  = new PPCClient(PPCHelper::getEnvironment($this->config));
        $hookURL = $this->getWebHookURL();

        if ($hookURL === null) {
            $errorString = 'createWebhook failed: webhook URL is empty';
            $this->logger->write(\LOGLEVEL_ERROR, $errorString);

            throw new WebhookException($errorString);
        }
        try {
            $webhookTypes = [];
            foreach (self::REGISTERED_EVENTS as $type) {
                $webhookTypes[] = (new EventType())->setType($type);
            }
            $response = new WebhookCreateResponse($client->send(
                new WebhookCreateRequest(Token::getInstance()->getToken(), $hookURL, $webhookTypes)
            ));
            $this->logger->write(\LOGLEVEL_DEBUG, 'createWebhook::WebhookCreateResponse:', $response);

            return $response->getWebhook();
        } catch (Exception | GuzzleException $e) {
            $errorString = 'createWebhook failed: ' . $e->getMessage();
            $this->logger->write(\LOGLEVEL_ERROR, $errorString);

            throw new WebhookException($errorString);
        }
    }

    /**
     * @param string $webhookId
     * @throws WebhookException
     */
    public function deleteWebhook(string $webhookId): void
    {
        $client = new PPCClient(PPCHelper::getEnvironment($this->config));
        try {
            $response = new WebhookDeleteResponse($client->send(
                new WebhookDeleteRequest(Token::getInstance()->getToken(), $webhookId)
            ));
            $this->config->removeWebhookId();
            $this->config->removeWebhookUrl();

            $this->logger->write(
                \LOGLEVEL_DEBUG,
                'deleteWebhook::WebhookDeleteResponse (204 expected) : ' . $response->getStatusCode(),
                $response
            );
        } catch (AuthorizationException | Exception | GuzzleException $e) {
            $errorString = 'deleteWebhook failed: ' . $e->getMessage();
            $this->logger->write(\LOGLEVEL_ERROR, $errorString);

            throw new WebhookException($errorString);
        }
    }

    /**
     * @return WebhookDetailsResponse
     * @throws WebhookException
     */
    public function loadWebhook(): WebhookDetailsResponse
    {
        $client    = new PPCClient(PPCHelper::getEnvironment($this->config));
        $webhookId = $this->config->getWebhookId();

        try {
            if ($webhookId === '') {
                $listResponse = new WebhookListResponse($client->send(
                    new WebhookListRequest(Token::getInstance()->getToken())
                ));
                $webhookUrl   = $this->getWebHookURL();
                $webhookId    = $listResponse->getId($webhookUrl) ?? '';
                $this->config->setWebhookId($webhookId);
                $this->config->setWebhookUrl($webhookUrl);
            }
            $detailResponse = new WebhookDetailsResponse($client->send(
                new WebhookDetailsRequest(Token::getInstance()->getToken(), $webhookId)
            ));
            $this->logger->write(\LOGLEVEL_DEBUG, 'loadWebhook::WebhookListResponse:', $detailResponse);

            $webhook = $detailResponse->getWebhook();
        } catch (Exception | GuzzleException $e) {
            $this->logger->write(\LOGLEVEL_ERROR, 'loadWebhook failed: ' . $e->getMessage());

            throw new WebhookException('loadWebhook failed: ' . $e->getMessage());
        }
        if ($webhook === null) {
            throw new WebhookException('loadWebhook failed: no data found for ' . $webhookId);
        }

        return $detailResponse;
    }

    /**
     * @param string      $webhookId
     * @param bool|string $content
     */
    public function handleCall(string $webhookId, bool|string $content): void
    {
        \ob_start();
        $this->checkDataValidation($content, $webhookId);

        $response     = new WebhookCallResponse($content);
        $eventType    = $response->getEventType() ?? 'unknown';
        $resourceType = $response->getResourceType() ?? '';

        $this->logger->write(\LOGLEVEL_DEBUG, 'Webhook::handleCall(' . $eventType . ') received: ', $response);
        $this->checkRegistration($eventType, $resourceType);
        $this->checkVerification($webhookId, $response);

        match ($resourceType) {
            'capture' => $this->handleCaptureWebhook($response, $eventType),
            'payment_token' => $this->handlePaymentTokenWebhook($response, $eventType),
        };
    }

    private function handleCaptureWebhook(WebhookCallResponse $response, string $eventType): void
    {
        try {
            $capture = new Capture($response->getData());
        } catch (JsonException | UnexpectedResponseException $e) {
            $this->logger->write(\LOGLEVEL_ERROR, 'Webhook::handleCall - Unexpected data for payment process id: '
                . $e->getMessage());

            $this->exitResult(400, 'Unexpected data for payment process id.');
            exit();
        }

        $db      = Shop::Container()->getDB();
        $txnId   = $capture->getRelatedOrderId();
        $payment = $db->getSingleObject(
            'SELECT tbestellung.kBestellung, COALESCE(tzahlungsid.kZahlungsart,
                    tbestellung.kZahlungsart) AS kZahlungsart,
                    tzahlungsid.cId, tzahlungsid.txn_id
                FROM tbestellung
                LEFT JOIN tzahlungsid ON tbestellung.kBestellung = tzahlungsid.kBestellung
                    AND tzahlungsid.txn_id = :txnId
                WHERE cBestellNr = :oderNumber',
            [
                'txnId'      => $txnId,
                'oderNumber' => $capture->getInvoiceId(),
            ]
        );

        if ($payment === null || (int)$payment->kZahlungsart === 0) {
            // payment process id not found
            // - there is no session hash created for the captured payment or payment is already processed
            $this->logger->write(
                \LOGLEVEL_NOTICE,
                'Webhook::handleCall - No payment for order id ' . $txnId . ' found'
            );

            $this->exitResult();
            exit();
        }

        $paymentHelper = Helper::getInstance($this->plugin);
        $payMethod     = $paymentHelper->getPaymentFromID((int)$payment->kZahlungsart);
        if ($payMethod === null) {
            $this->logger->write(
                \LOGLEVEL_NOTICE,
                'Webhook::handleCall - No payment method for order id ' . $txnId . ' found'
            );

            $this->exitResult();
            exit();
        }
        $this->logger->setMethod($payMethod);

        if (!$payMethod->handleCaptureWebhook($eventType, $capture, $payment)) {
            $this->logger->write(
                \LOGLEVEL_ERROR,
                'Webhook::handleCall - capture failed for method ' . $payMethod->getMethod()->getName()
            );
        }

        $this->exitResult();
        exit();
    }

    private function handlePaymentTokenWebhook(WebhookCallResponse $response, string $eventType): void
    {
        if ($eventType !== EventType::VAULT_TOKEN_DELETED) {
            $this->logger->write(\LOGLEVEL_DEBUG, \sprintf(
                'Webhook::handlePaymentTokenWebhook - "%s" not supported',
                $eventType
            ));
            $this->exitResult();
            exit();
        }

        try {
            $tokenId = $response->getData()->id ?? null;
        } catch (JsonException | UnexpectedResponseException) {
            $tokenId = null;
        }
        if ($tokenId === null) {
            $this->logger->write(\LOGLEVEL_ERROR, 'Webhook::handlePaymentTokenWebhook - Unexpected token id');

            $this->exitResult(400, 'Unexpected token id.');
            exit();
        }

        $vaultHelper = new VaultingHelper($this->config);
        $vaultHelper->deleteVault($tokenId);

        $this->exitResult();
        exit();
    }
}
