<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\frontend\Handler;

use DateTime;
use Exception;
use JTL\Checkout\Bestellung;
use JTL\DB\DbInterface;
use JTL\Plugin\PluginInterface;
use JTL\Shop;
use Plugin\jtl_paypal_commerce\paymentmethod\Helper;

/**
 * Class OrderHandler
 * @package Plugin\jtl_paypal_commerce\frontend\Handler
 */
class OrderHandler
{
    private PluginInterface $plugin;
    private DbInterface $db;

    /**
     * OrderHandler constructor
     */
    public function __construct(PluginInterface $plugin, ?DbInterface $db = null)
    {
        $this->plugin = $plugin;
        $this->db     = $db ?? Shop::Container()->getDB();
    }

    public function saveOrder(array $args): void
    {
        /** @var Bestellung $order */
        $order  = $args['oBestellung'];
        $helper = Helper::getInstance($this->plugin);

        $payMethod = $helper->getPaymentFromID((int)$order->kZahlungsart);
        if ($payMethod !== null) {
            $payMethod->finalizeOrderInDB($order);
        }
    }

    /**
     * @throws Exception
     */
    public function updateOrder(array $args): void
    {
        /** @var Bestellung $order */
        $order = $args['oBestellung'];
        $wawi  = $args['oBestellungWawi'];
        $sent  = empty($wawi->dVersandt) ? null : new DateTime($wawi->dVersandt);
        $now   = new DateTime();

        if (
            empty($wawi->cIdentCode)
            || $sent === null
            || $now->diff($sent)->format('%a') > \BESTELLUNG_VERSANDBESTAETIGUNG_MAX_TAGE
        ) {
            return;
        }
        $paymentIds = [];
        foreach ($this->plugin->getPaymentMethods()->getMethods() as $method) {
            $paymentIds[] = $method->getMethodID();
        }
        $payments = $this->db->getCollection(
            'SELECT tbestellung.kBestellung, COALESCE(cMap.carrier_paypal, tbestellung.cLogistiker) AS carrier,
                    tzahlungseingang.cHinweis AS transaction_id,
                    tbestellung.dVersandDatum AS shipmentDate,
                    ADDDATE(tbestellung.dVersandDatum, INTERVAL tversandart.nMaxLiefertage DAY) AS deliveryDate
                FROM tbestellung
                INNER JOIN tzahlungseingang
                    ON tzahlungseingang.kBestellung = tbestellung.kBestellung
                LEFT JOIN xplugin_jtl_paypal_checkout_carrier_mapping AS cMap
                    ON cMap.carrier_wawi = tbestellung.cLogistiker
                INNER JOIN tversandart
                    ON tversandart.kVersandart = tbestellung.kVersandart
                WHERE tbestellung.kBestellung = :orderId
                    AND tbestellung.kZahlungsart IN (' . \implode(', ', $paymentIds) . ")
                    AND tzahlungseingang.cHinweis RLIKE '^[A-Z0-9]+$'",
            [
                'orderId' => $order->kBestellung,
            ]
        );
        foreach ($payments as $payment) {
            $this->db->upsert('xplugin_jtl_paypal_checkout_shipment_state', (object)[
                'transaction_id' => $payment->transaction_id,
                'tracking_id'    => $wawi->cIdentCode,
                'carrier'        => $payment->carrier,
                'shipment_date'  => $payment->shipmentDate,
                'delivery_date'  => $payment->deliveryDate,
                'status_sent'    => 0,
            ], ['transaction_id']);
        }
    }
}
