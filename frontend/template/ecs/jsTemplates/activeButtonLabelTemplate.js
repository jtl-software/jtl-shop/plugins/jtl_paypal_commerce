const activeButtonLabelTemplate = ({activeButtonLabel}) => `
<span class="ppc-button-placeholder-active">
    <span>${activeButtonLabel}</span> <i class="fa fa-chevron-down"></i>
</span>
`