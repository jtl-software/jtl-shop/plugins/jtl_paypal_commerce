let instalmentBannerPlaceholderTemplate = ({ppcConsentPlaceholder}) =>
    `<button type="button" class="btn btn-outline-secondary btn-block">${ppcConsentPlaceholder}</button>`;