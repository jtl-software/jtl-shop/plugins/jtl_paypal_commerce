<?php

namespace Plugin\jtl_paypal_commerce\tests;

use Exception;
use Gettext\Translator;
use Gettext\TranslatorFunctions;
use JTL\Shop;
use JTL\Template\Model;
use JTL\Template\TemplateService;
use JTL\Template\TemplateServiceInterface;
use Plugin\jtl_paypal_commerce\PPC\BackendUIsettings;
use Tests\UnitTestCase;

class BackendUIsettingsTest extends UnitTestCase
{
    /**
     * @inheritDoc
     * @throws Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        $templateModel = new Model();
        $templateModel->setName('NOVA');

        $templateService = $this->createMock(TemplateService::class);
        $templateService->method('getActiveTemplate')->willReturn($templateModel);

        Shop::Container()->setSingleton(TemplateServiceInterface::class, function () use ($templateService) {
            return $templateService;
        });

        TranslatorFunctions::register(new Translator());
    }

    /**
     * @throws Exception
     */
    public function testGetDefaultSettings(): void
    {
        $defaults = \json_decode(
            \trim(file_get_contents(__DIR__ . '/defaultSettings.json')),
            false,
            512,
            \JSON_THROW_ON_ERROR
        );
        $settings = BackendUIsettings::getDefaultSettings();
        self::assertEquals(
            \json_encode($defaults, \JSON_THROW_ON_ERROR),
            \json_encode($settings, \JSON_THROW_ON_ERROR)
        );
    }
}
