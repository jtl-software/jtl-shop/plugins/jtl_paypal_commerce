<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment;

use DateTime;
use InvalidArgumentException;
use JsonException;
use PHPUnit\Framework\TestCase;
use Plugin\jtl_paypal_commerce\PPC\Order\Address;
use Plugin\jtl_paypal_commerce\PPC\Order\ExperienceContext;
use Plugin\jtl_paypal_commerce\PPC\Order\Payer;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\AbstractPaymentSource;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\PaymentSourceInterface;
use Plugin\jtl_paypal_commerce\PPC\Order\Phone;
use Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment\TestSources\TestPayerPaymentSource;
use Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment\TestSources\TestPaymentSource;
use stdClass;

/**
 * Class AbstractPaymentSourceTest
 * @package PPC\Order\Payment
 */
class AbstractPaymentSourceTest extends TestCase
{
    private string $data;

    /**
     * @throws JsonException
     */
    protected function setUp(): void
    {
        parent::setUp();

        $data          = new stdClass();
        $data->my_name = 'Tom Tester';
        $data->email   = 'tom@test.com';

        $this->data = \json_encode($data, \JSON_THROW_ON_ERROR | \JSON_UNESCAPED_UNICODE);
    }

    /**
     * @throws JsonException
     */
    private function getPaymentSource(?object $data = null, array $supportedProps = []): PaymentSourceInterface
    {
        if ($data === null) {
            return new TestPaymentSource(\json_decode($this->data, false, 512, \JSON_THROW_ON_ERROR));
        }

        return new AbstractPaymentSource($data, $supportedProps);
    }

    /**
     * @throws JsonException
     */
    public function testInstanceCreation(): TestPaymentSource
    {
        $paymentSource = $this->getPaymentSource();
        $this->assertInstanceOf(TestPaymentSource::class, $paymentSource);

        return $paymentSource;
    }

    /**
     * @throws JsonException
     */
    public function testIsPropertyActive(): void
    {
        $data                     = new stdClass();
        $data->name               = 'Tom Tester';
        $data->email              = 'tom@test.com';
        $data->experience_context = new ExperienceContext();

        $paymentSource = $this->getPaymentSource($data);
        $this->assertEquals(
            '{'
            . '"name":"Tom Tester",'
            . '"email":"tom@test.com",'
            . '"experience_context":{'
                . '"locale":"de-DE"'
            . '}}',
            (string)$paymentSource
        );
        $this->assertTrue($paymentSource->isPropertyActive('name'));
        $this->assertTrue($paymentSource->isPropertyActive('vault_id'));

        $paymentSource = $this->getPaymentSource($data, ['name', 'email']);
        $this->assertEquals(
            '{'
            . '"name":"Tom Tester",'
            . '"email":"tom@test.com"'
            . '}',
            (string)$paymentSource
        );
        $this->assertTrue($paymentSource->isPropertyActive('name'));
        $this->assertFalse($paymentSource->isPropertyActive('vault_id'));

        $paymentSource = $this->getPaymentSource();
        $this->assertEquals(
            '{'
            . '"my_name":"MY:Tom Tester",'
            . '"email":"tom@test.com"'
            . '}',
            (string)$paymentSource
        );
        $this->assertTrue($paymentSource->isPropertyActive('email'));
        $this->assertTrue($paymentSource->isPropertyActive('my_name'));
        $this->assertTrue($paymentSource->isPropertyActive('email_address'));
    }

    public function testInvalidateProperty(): void
    {
        $paymentSource = new AbstractPaymentSource(null, ['name']);
        $paymentSource->invalidateProperty('email_address');

        $this->expectException(InvalidArgumentException::class);
        $paymentSource->invalidateProperty('name');
    }

    public function testSetData(): void
    {
        $data                     = new stdClass();
        $data->my_name            = 'Tom Tester';
        $data->email              = 'tom@test.com';
        $data->experience_context = new ExperienceContext();

        $paymentSource = new TestPaymentSource($data);
        $this->assertInstanceOf(ExperienceContext::class, $paymentSource->getData()->experience_context);
        $this->assertEquals(
            '{'
            . '"my_name":"MY:Tom Tester",'
            . '"email":"tom@test.com",'
            . '"experience_context":{'
                . '"locale":"de-DE"'
            . '}}',
            (string)$paymentSource
        );
    }

    public function testSetEmpty(): void
    {
        $data          = new stdClass();
        $data->my_name = 'Tom Tester';
        $data->email   = '';

        $paymentSource = new TestPaymentSource($data);
        $this->assertEquals(
            '{'
            . '"my_name":"MY:Tom Tester"'
            . '}',
            (string)$paymentSource
        );
    }

    /**
     * @throws JsonException
     */
    public function testJsonSerialize(): void
    {
        $paymentSource = new AbstractPaymentSource(\json_decode($this->data, false, 512, \JSON_THROW_ON_ERROR));
        $this->assertEquals($this->data, (string)$paymentSource);
    }

    public function testSetPayer(): void
    {
        $paymentSource = new TestPayerPaymentSource();
        $payer         = (new Payer())
            ->setGivenName('Tom')
            ->setSurname('Tester')
            ->setEmail('tom.tester@test.com')
            ->setBirthDate(new DateTime('2000-10-14'))
            ->setPhone((new Phone())->setNumber('004945698712'))
            ->setAddress((new Address())
                ->setAddress(['Große Ullrichstr. 49', 'über Spiegelstr.'])
                ->setCountryCode('DE')
                ->setCity('Halle (Saale)')
                ->setPostalCode('06108'));
        $paymentSource->applyPayer($payer);
        $this->assertEquals('Tom Tester', $paymentSource->getName());
        $this->assertEquals('2000-10-14', $paymentSource->getBirthDate()->format('Y-m-d'));
        $this->assertEquals('004945698712', $paymentSource->getPhoneNumber()->getNumber('00', ''));
        $this->assertEquals('Große Ullrichstr. 49', $paymentSource->getBillingAddress()->getAddress()[0]);
        $this->assertEquals('über Spiegelstr.', $paymentSource->getBillingAddress()->getAddress()[1]);
        $this->assertEquals('DE', $paymentSource->getBillingAddress()->getCountryCode());
        $this->assertEquals('Halle (Saale)', $paymentSource->getBillingAddress()->getCity());
        $this->assertEquals('06108', $paymentSource->getBillingAddress()->getPostalCode());
        $this->assertEquals(
            '{'
            . '"name":"Tom Tester",'
            . '"email_address":"tom.tester@test.com",'
            . '"birth_date":"2000-10-14",'
            . '"phone_number":{"national_number":"004945698712"},'
            . '"address":{'
                . '"address_line_1":"Große Ullrichstr. 49",'
                . '"address_line_2":"über Spiegelstr.",'
                . '"country_code":"DE",'
                . '"admin_area_2":"Halle (Saale)",'
                . '"postal_code":"06108"'
                . '}'
            . '}',
            (string)$paymentSource
        );
    }

    public function testGetPayer(): void
    {
        $paymentSource = new TestPayerPaymentSource();
        $paymentSource->setName('Tom Tester');
        $paymentSource->setEmail('tom.tester@test.com');
        $paymentSource->setBirthDate(new DateTime('2000-10-14'));
        $paymentSource->setPhoneNumber((new Phone())->setNumber('004945698712'));
        $paymentSource->setBillingAddress((new Address())
            ->setAddress(['Große Ullrichstr. 49', 'über Spiegelstr.'])
            ->setCountryCode('DE')
            ->setCity('Halle (Saale)')
            ->setPostalCode('06108'));
        $payer = $paymentSource->fetchPayer();
        $this->assertEquals('Tom', $payer->getGivenName());
        $this->assertEquals('Tester', $payer->getSurname());
        $this->assertEquals('tom.tester@test.com', $payer->getEmail());
        $this->assertEquals('2000-10-14', $payer->getBirthDate()->format('Y-m-d'));
        $this->assertEquals('004945698712', $payer->getPhone()->getNumber('00', ''));
        $address = $payer->getAddress();
        $this->assertInstanceOf(Address::class, $address);
        $this->assertEquals('Große Ullrichstr. 49', $address->getAddress()[0]);
        $this->assertEquals('über Spiegelstr.', $address->getAddress()[1]);
        $this->assertEquals('DE', $address->getCountryCode());
        $this->assertEquals('Halle (Saale)', $address->getCity());
        $this->assertEquals('06108', $address->getPostalCode());
    }

    /**
     * @throws JsonException
     */
    public function testGetPayerInitial(): void
    {
        $paymentSource = new TestPayerPaymentSource(
            \json_decode(
                '{'
                . '"name":"Tom Tester",'
                . '"email_address":"tom.tester@test.com",'
                . '"birth_date":"2000-10-14",'
                . '"phone_number":{"national_number":"004945698712"},'
                . '"address":{'
                    . '"address_line_1":"Große Ullrichstr. 49",'
                    . '"address_line_2":"über Spiegelstr.",'
                    . '"country_code":"DE",'
                    . '"admin_area_2":"Halle (Saale)",'
                    . '"postal_code":"06108"'
                    . '}'
                . '}',
                false,
                512,
                JSON_THROW_ON_ERROR
            )
        );

        $payer = $paymentSource->fetchPayer();
        $this->assertEquals('Tom', $payer->getGivenName());
        $this->assertEquals('Tester', $payer->getSurname());
        $this->assertEquals('tom.tester@test.com', $payer->getEmail());
        $this->assertEquals('2000-10-14', $payer->getBirthDate()->format('Y-m-d'));
        $this->assertEquals('004945698712', $payer->getPhone()->getNumber('00', ''));
        $address = $payer->getAddress();
        $this->assertInstanceOf(Address::class, $address);
        $this->assertEquals('Große Ullrichstr. 49', $address->getAddress()[0]);
        $this->assertEquals('über Spiegelstr.', $address->getAddress()[1]);
        $this->assertEquals('DE', $address->getCountryCode());
        $this->assertEquals('Halle (Saale)', $address->getCity());
        $this->assertEquals('06108', $address->getPostalCode());
    }
}
