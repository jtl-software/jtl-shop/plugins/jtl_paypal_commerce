<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment\TestSources\PropertyBillingAgreementIdPaymentSource;

/**
 * Class PropertyBillingAgreementIdTest
 * @package PPC\Order\Payment
 */
class PropertyBillingAgreementIdTest extends TestCase
{
    public function testSetGetPropertyBillingAgreementId(): void
    {
        $paymentSource = new PropertyBillingAgreementIdPaymentSource((object)['billing_agreement_id' => 'abcFGH674kl']);
        $this->assertEquals('abcFGH674kl', $paymentSource->getBillingAgreementId());
        $instance = $paymentSource->setBillingAgreementId('abcFGH0-00674kl');
        $this->assertInstanceOf(PropertyBillingAgreementIdPaymentSource::class, $instance);
        $this->assertEquals('abcFGH0-00674kl', $instance->getBillingAgreementId());

        $this->expectException(InvalidArgumentException::class);
        $paymentSource->setBillingAgreementId('abcFGH#000674kl');
    }
}
