<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment;

use PHPUnit\Framework\TestCase;
use Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment\TestSources\PropertyEmailAdressPaymentSource;

/**
 * Class PropertyEmailAdressTest
 * @package PPC\Order\Payment
 */
class PropertyEmailAdressTest extends TestCase
{
    public function testGetEmail(): void
    {
        $paymentSource = new PropertyEmailAdressPaymentSource((object)['email_address' => 'karla.kolumna@test.com']);
        $this->assertEquals('karla.kolumna@test.com', $paymentSource->getEmail());
        $instance = $paymentSource->setEmail('karla.kolumna@columne.com');
        $this->assertEquals('karla.kolumna@columne.com', $instance->getEmail());
    }
}
