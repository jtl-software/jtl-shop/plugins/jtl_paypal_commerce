<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\PaymentSourceProperties\PropertyConstants;
use Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment\TestSources\PropertyTypePaymentSource;

/**
 * Class PropertyTypeTest
 * @package PPC\Order\Payment
 */
class PropertyTypeTest extends TestCase
{
    public function testSetGetType(): void
    {
        $paymentSource = new PropertyTypePaymentSource((object)['type' => PropertyConstants::TYPE_BILLING_AGREEMENT]);
        $this->assertEquals(PropertyConstants::TYPE_BILLING_AGREEMENT, $paymentSource->getType());

        $this->expectException(InvalidArgumentException::class);
        $paymentSource->setType('Irgendwas');
    }
}
