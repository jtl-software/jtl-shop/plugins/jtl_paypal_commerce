<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment;

use JsonException;
use PHPUnit\Framework\TestCase;
use Plugin\jtl_paypal_commerce\PPC\Order\ExperienceContext;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\PaymentSourceBuilder;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\PayPalPaymentSource;
use Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment\TestSources\PayPalPaymentData;

class PayPalPaymentSourceTest extends TestCase
{
    private object $data;

    protected function setUp(): void
    {
        parent::setUp();

        $experienceContext = (object)[
            'brand_name'                => 'Mein schöner Shop',
            'shipping_preference'       => ExperienceContext::SHIPPING_FROM_FILE,
            'landing_page'              => ExperienceContext::PAGE_NO_PREFERENCE,
            'user_action'               => ExperienceContext::USER_ACTION_CONTINUE,
            'payment_method_preference' => ExperienceContext::METHOD_UNRESTRICTED,
            'locale'                    => 'de-DE',
            'return_url'                => 'https://example.com/return',
            'cancel_url'                => 'https://example.com/cancel',
        ];
        $address           = (object)[
            'country_code'   => 'DE',
            'address_line_1' => 'Große Ullrichstr. 49',
            'address_line_2' => 'über Spiegelstr.',
            'admin_area_1'   => 'Sachsen-Anhalt',
            'admin_area_2'   => 'Halle',
            'postal_code'    => '06108',
        ];
        $attributes        = (object)[
            'customer' => (object)[
                'id'            => '1234ABC-xyz',
                'email_address' => 'merchant@example.com',
            ],
            'vault'    => (object)[
                'store_in_vault' => 'ON_SUCCESS',
                'customer_type'  => 'CONSUMER',
            ],
        ];
        $this->data        = (object)[
            'experience_context' => $experienceContext,
            'billing_agreement_id' => 'ABC-xyz123',
            'vault_id' => 'XYZ_vgf-123',
            'email_address' => 'tom.tester@example.com',
            'name' => (object)[
                'given_name' => 'Tester',
                'surname'    => 'Tom',
            ],
            'phone' => (object)[
                'phone_number' => (object)[
                    'national_number' => '0049123456789',
                ],
            ],
            'birth_date' => '1975-04-01',
            'address' => $address,
            'attributes' => $attributes,
        ];
    }

    public function testInstanceCreation(): void
    {
        $paymentSource = new PayPalPaymentSource($this->data);
        $this->assertInstanceOf(PayPalPaymentSource::class, $paymentSource);
    }

    public function testPhoneNumber(): void
    {
        $paymentSource = new PayPalPaymentSource($this->data);
        $phoneNumber   = $paymentSource->getPhoneNumber();
        $this->assertEquals('+49 123456789', $phoneNumber->getNumber());

        $paymentSource = new PayPalPaymentSource();
        $phoneNumber->setNationalNumber('6912345678');
        $paymentSource->setPhoneNumber($phoneNumber);
        $this->assertEquals('{"phone":{"phone_number":{"national_number":"00496912345678"}}}', (string)$paymentSource);
    }

    /**
     * @throws JsonException
     */
    public function testJsonSerialize(): void
    {
        $data          = clone $this->data;
        $paymentSource = new PayPalPaymentSource(clone $data);
        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );

        $data->billing_agreement_id = 'ABC-xyz345';
        $paymentSource->setBillingAgreementId($data->billing_agreement_id);
        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );

        $data->vault_id = 'XYZ_vgf-456';
        $paymentSource->setVaultid($data->vault_id);
        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );

        $data->email_address = 'tom.tester@nirgendwo.com';
        $paymentSource->setEmail($data->email_address);
        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );

        $paymentSource->setExperienceContext();
        unset($data->experience_context);
        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );

        $paymentSource->setAttributes();
        unset($data->attributes);
        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );

        $paymentSource->setPhoneNumber()
            ->setEmail('')
            ->setName('')
            ->setBirthDate()
            ->setBillingAddress();
        unset(
            $data->phone,
            $data->email_address,
            $data->name,
            $data->address,
            $data->birth_date
        );

        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );
    }

    /**
     * @throws JsonException
     */
    public function testCreation(): void
    {
        $paymentData   = new PayPalPaymentData(PaymentSourceBuilder::FUNDING_PAYPAL);
        $paymentSource = $paymentData->build();
        $experienceCtx = $paymentSource->getExperienceContext();
        $payer         = $paymentSource->fetchPayer();
        $address       = $payer->getAddress();
        $expectedJSON  = \sprintf(
            '{
                "name":{
                    "given_name":"%s",
                    "surname":"%s"
                },
                "email_address":"%s",
                "birth_date":"%s",
                "phone":{"phone_number":{"national_number":"%s"}},
                "address":{
                    "address_line_1":"%s",
                    "address_line_2":"%s",
                    "country_code":"%s",
                    "postal_code":"%s",
                    "admin_area_2":"%s"
                },
                "experience_context":{
                    "locale":"%s",
                    "brand_name":"%s",
                    "shipping_preference":"%s",
                    "user_action":"%s"
                }
            }',
            $payer->getGivenName(),
            $payer->getSurname(),
            $payer->getEmail(),
            $payer->getBirthDate()->format('Y-m-d'),
            $payer->getPhone()->getNumber('00', ''),
            $address->getAddress()[0],
            $address->getAddress()[1],
            $address->getCountryCode(),
            $address->getPostalCode(),
            $address->getCity(),
            $experienceCtx->getLocale(),
            $experienceCtx->getBrandName(),
            $experienceCtx->getShippingPreference(),
            $experienceCtx->getUserAction(),
        );

        $this->assertEquals(
            \json_encode(
                \json_decode($expectedJSON, null, 512, JSON_THROW_ON_ERROR),
                JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES
            ),
            (string)$paymentSource
        );
    }

    public function testAttributeVault(): void
    {
        $paymentSource = (new PayPalPaymentSource())->withVaultRequest();
        $this->assertEquals(
            '{'
            . '"attributes":{'
                . '"vault":{'
                    . '"permit_multiple_payment_tokens":"false",'
                    . '"store_in_vault":"ON_SUCCESS",'
                    . '"usage_type":"MERCHANT",'
                    . '"customer_type":"CONSUMER"'
                . '}'
            . '}}',
            (string)$paymentSource
        );
        $paymentSource->withVaultRequest(false);
        $this->assertEquals('{}', (string)$paymentSource);
    }
}
