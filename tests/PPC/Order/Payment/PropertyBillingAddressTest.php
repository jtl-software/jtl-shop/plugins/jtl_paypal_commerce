<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment;

use PHPUnit\Framework\TestCase;
use Plugin\jtl_paypal_commerce\PPC\Order\Address;
use Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment\TestSources\PropertyBillingAdressPaymentSource;

class PropertyBillingAddressTest extends TestCase
{
    public function testSetGetBillingAddress(): void
    {
        $paymentSource = new PropertyBillingAdressPaymentSource(
            (object)['billing_address' => (object)[
                'country_code'   => 'DE',
                'address_line_1' => 'Große Ullrichstr. 49',
                'address_line_2' => 'über Spiegelstr.',
                'admin_area_2'   => 'Halle',
                'postal_code'    => '06108',
            ]]
        );

        $address = $paymentSource->getBillingAddress();
        $this->assertInstanceOf(Address::class, $address);
        $this->assertEquals('Halle', $address->getCity());
        $this->assertEquals('DE', $address->getCountryCode());
        $this->assertEquals('06108', $address->getPostalCode());
        $this->assertEquals('Große Ullrichstr. 49', $address->getAddress()[0]);
        $this->assertEquals('über Spiegelstr.', $address->getAddress()[1]);

        $address->setCity('Hückelhoven')
                ->setPostalCode('41836')
                ->setAddress(['Rheinstr. 51']);

        $instance = $paymentSource->setBillingAddress($address);
        $this->assertInstanceOf(PropertyBillingAdressPaymentSource::class, $instance);
        $this->assertEquals('Hückelhoven', $address->getCity());
        $this->assertEquals('DE', $address->getCountryCode());
        $this->assertEquals('41836', $address->getPostalCode());
        $this->assertEquals('Rheinstr. 51', $address->getAddress()[0]);
        $this->assertEquals('', $address->getAddress()[1] ?? '');
    }
}
