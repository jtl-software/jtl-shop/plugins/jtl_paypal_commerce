<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment;

use InvalidArgumentException;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\NetworkTransactionReference;
use PHPUnit\Framework\TestCase;

class NetworkTransactionReferenceTest extends TestCase
{
    public function testInstanceCreation(): NetworkTransactionReference
    {
        $netTransRef = new NetworkTransactionReference((object)['id' => 'abcfd123JKL']);
        $this->assertInstanceOf(NetworkTransactionReference::class, $netTransRef);

        return $netTransRef;
    }

    /**
     * @depends testInstanceCreation
     * @param NetworkTransactionReference $netTransRef
     * @return void
     */
    public function testSetGetId(NetworkTransactionReference $netTransRef): void
    {
        $this->assertEquals('abcfd123JKL', $netTransRef->getId());
        $this->assertEquals('abcfd345JKL', $netTransRef->setId('abcfd345JKL')->getId());

        $this->expectException(InvalidArgumentException::class);
        $netTransRef->setId('abcfdKL');
    }

    /**
     * @depends testInstanceCreation
     * @param NetworkTransactionReference $netTransRef
     * @return void
     */
    public function testSetGetDate(NetworkTransactionReference $netTransRef): void
    {
        $this->assertEquals('abcd', $netTransRef->setDate('abcd')->getDate());

        $this->expectException(InvalidArgumentException::class);
        $netTransRef->setDate('abcde');
    }

    /**
     * @depends testInstanceCreation
     * @param NetworkTransactionReference $netTransRef
     * @return void
     */
    public function testSetGetRefNumber(NetworkTransactionReference $netTransRef): void
    {
        $this->assertEquals('abc', $netTransRef->setRefNumber('abc')->getRefNumber());

        $this->expectException(InvalidArgumentException::class);
        $netTransRef->setRefNumber('');
    }

    /**
     * @depends testInstanceCreation
     * @param NetworkTransactionReference $netTransRef
     * @return void
     */
    public function testSetGetNetwork(NetworkTransactionReference $netTransRef): void
    {
        $this->assertEquals('VISA', $netTransRef->setNetwork('VISA')->getNetwork());

        $this->expectException(InvalidArgumentException::class);
        $netTransRef->setNetwork('visa');
    }

    public function testJsonSerialize(): void
    {
        $netTransRef = new NetworkTransactionReference((object)[
            'id'                        => 'abcfd345JKL#',
            'acquirer_reference_number' => '12345',
            'date'                      => 'DFGH',
            'network'                   => 'MASTERCARD',
        ]);
        $this->assertEquals(
            '{"id":"abcfd345JKL#","acquirer_reference_number":"12345","date":"DFGH","network":"MASTERCARD"}',
            (string)$netTransRef
        );
        $this->assertEquals(
            '{"id":"abcfd345JKL#","acquirer_reference_number":"12345","network":"MASTERCARD"}',
            (string)$netTransRef->setDate(null)
        );
        $this->assertEquals(
            '{"id":"abcfd345JKL#","network":"MASTERCARD"}',
            (string)$netTransRef->setRefNumber(null)
        );
        $this->assertEquals(
            '{"id":"abcfd345JKL#"}',
            (string)$netTransRef->setNetwork(null)
        );
    }
}
