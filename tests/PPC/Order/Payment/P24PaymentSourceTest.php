<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment;

use JsonException;
use PHPUnit\Framework\TestCase;
use Plugin\jtl_paypal_commerce\PPC\Order\ExperienceContext;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\P24PaymentSource;

/**
 * Class P24PaymentSourceTest
 * @package PPC\Order\Payment
 */
class P24PaymentSourceTest extends TestCase
{
    private object $data;

    protected function setUp(): void
    {
        parent::setUp();

        $experienceContext = (object)[
            'brand_name'                => 'Mein schöner Shop',
            'shipping_preference'       => ExperienceContext::SHIPPING_FROM_FILE,
            'locale'                    => 'de-DE',
            'return_url'                => 'https://example.com/return',
            'cancel_url'                => 'https://example.com/cancel',
        ];
        $this->data        = (object)[
            'name'               => 'Toni Turbo jr.',
            'email'              => 'toni.turbojr@example.de',
            'country_code'       => 'DE',
            'experience_context' => $experienceContext,
        ];
    }

    public function testInstanceCreation(): void
    {
        $paymentSource = new P24PaymentSource($this->data);
        $this->assertInstanceOf(P24PaymentSource::class, $paymentSource);
    }

    /**
     * @throws JsonException
     */
    public function testJsonSerialize(): void
    {
        $data          = clone $this->data;
        $paymentSource = new P24PaymentSource(clone $data);
        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );

        $paymentSource->setName('Achim van Gröne');
        $data->name = 'Achim van Gröne';
        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );

        $paymentSource->setCountryCode('EN');
        $data->country_code = 'EN';
        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );

        $paymentSource->setExperienceContext();
        unset($data->experience_context);
        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );

        $paymentSource->setEmail('toni.turbojr@mymail.com');
        $this->assertEquals(
            '{"name":"Achim van Gröne","email":"toni.turbojr@mymail.com","country_code":"EN"}',
            (string)$paymentSource
        );
    }

    /**
     * @throws JsonException
     */
    public function testInvalidJsonSerialize(): void
    {
        $data           = clone $this->data;
        $data->vault_id = 'ABC_123';
        $paymentSource  = new P24PaymentSource($data);
        $this->assertEquals(
            \json_encode($this->data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );
    }
}
