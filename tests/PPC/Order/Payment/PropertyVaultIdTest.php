<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment\TestSources\PropertyVaultIdPaymentSource;

class PropertyVaultIdTest extends TestCase
{
    public function testSetGetVaultId(): void
    {
        $paymentSource = new PropertyVaultIdPaymentSource((object)['vault_id' => 'xyzHGFD6345']);
        $this->assertEquals('xyzHGFD6345', $paymentSource->getVaultId());
        $paymentSource->setVaultId('abc_FGH-674kl');
        $this->assertEquals('abc_FGH-674kl', $paymentSource->getVaultId());

        $this->expectException(InvalidArgumentException::class);
        $paymentSource->setVaultId('abc#FGH-674kl');
    }
}
