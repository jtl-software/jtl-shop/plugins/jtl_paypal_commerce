<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment;

use PHPUnit\Framework\TestCase;
use Plugin\jtl_paypal_commerce\PPC\Order\ExperienceContext;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\VenmoPaymentSource;
use Plugin\jtl_paypal_commerce\PPC\Request\Serializer\JSON;

/**
 * Class VenmoPaymentSourceTest
 * @package PPC\Order\Payment
 */
class VenmoPaymentSourceTest extends TestCase
{
    private object $data;

    protected function setUp(): void
    {
        parent::setUp();

        $attributes        = (object)[
            'customer' => (object)[
                'id'            => '1234ABC-xyz',
                'email_address' => 'merchant@example.com',
            ],
            'vault'    => (object)[
                'store_in_vault' => 'ON_SUCCESS',
                'usage_type'     => 'KLOUZT_123'
            ],
        ];
        $experienceContext = (object)[
            'brand_name'                => 'Mein schöner Shop',
            'shipping_preference'       => ExperienceContext::SHIPPING_FROM_FILE,
        ];
        $this->data        = (object)[
            'experience_context' => $experienceContext,
            'vault_id'           => 'ABCFD-4567',
            'email_address'      => 'max.mustermann@fakemail.com',
            'attributes'         => $attributes,
        ];
    }

    public function testInstanceCreation(): void
    {
        $paymentSource = new VenmoPaymentSource($this->data);
        $this->assertInstanceOf(VenmoPaymentSource::class, $paymentSource);
    }

    /**
     * @throws \JsonException
     */
    public function testJsonSerialize(): void
    {
        $data          = clone $this->data;
        $paymentSource = new VenmoPaymentSource(clone $data);
        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );

        $data->email_address = 'customer@example.com';
        $paymentSource->setEmail($data->email_address);
        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );

        $data->vault_id = '485662245';
        $paymentSource->setVaultId($data->vault_id);
        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );

        $paymentSource->setExperienceContext();
        unset($data->experience_context);
        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );
    }

    public function testSetGetAttributes(): void
    {
        $paymentSource = new VenmoPaymentSource(clone $this->data);
        $this->assertEquals('1234ABC-xyz', $paymentSource->getAttribute('customer')->getData()->id);
        $vault = $paymentSource->getAttribute('vault')->getData();
        $this->assertEquals('ON_SUCCESS', $vault->store_in_vault);
        $vault->store_in_vault = 'ON_FAILURE';
        $paymentSource->addAttribute('vault', new JSON($vault));
        $this->assertEquals(
            '{'
                . '"customer":{'
                    . '"id":"1234ABC-xyz",'
                    . '"email_address":"merchant@example.com"'
                . '},'
                . '"vault":{'
                    . '"store_in_vault":"ON_FAILURE",'
                    . '"usage_type":"KLOUZT_123"'
                . '}'
            . '}',
            (string)$paymentSource->getAttributes()
        );
    }
}
