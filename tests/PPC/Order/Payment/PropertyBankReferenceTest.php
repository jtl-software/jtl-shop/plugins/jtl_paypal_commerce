<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment;

use PHPUnit\Framework\TestCase;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\BankDetails;
use Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment\TestSources\PropertyBankReferencePaymentSource;

/**
 * Class PropertyBankReferenceTest
 * @package PPC\Order\Payment
 */
class PropertyBankReferenceTest extends TestCase
{
    public function testGetPaymentReference(): void
    {
        $paymentSource = new PropertyBankReferencePaymentSource((object)[
            'payment_reference' => 'b8a1525dlYzu6Mn62umI'
        ]);
        $this->assertEquals('b8a1525dlYzu6Mn62umI', $paymentSource->getPaymentReference());
        $this->assertEquals('{"payment_reference":"b8a1525dlYzu6Mn62umI"}', (string)$paymentSource);
    }

    public function testGetBankDetails(): void
    {
        $paymentSource = new PropertyBankReferencePaymentSource((object)[
            'deposit_bank_details' => (object)[
                "bic"                 => "DEUTDEFFXXX",
                "bank_name"           => "Deutsche Bank",
                "iban"                => "DE89370400440532013000",
                "account_holder_name" => "Paypal - Ratepay GmbH - Test Bank Account",
            ]]);
        $bankDetails   = $paymentSource->getDepositBankDetails();
        $this->assertInstanceOf(BankDetails::class, $bankDetails);
        $this->assertEquals('DEUTDEFFXXX', $bankDetails->getBIC());
        $this->assertEquals('DE89370400440532013000', $bankDetails->getIBAN());
        $this->assertEquals('Paypal - Ratepay GmbH - Test Bank Account', $bankDetails->getAccountHolder());
        $this->assertEquals('Deutsche Bank', $bankDetails->getBankName());
        $this->assertEquals(
            '{'
            . '"deposit_bank_details":{'
                . '"bic":"DEUTDEFFXXX",'
                . '"bank_name":"Deutsche Bank",'
                . '"iban":"DE89370400440532013000",'
                . '"account_holder_name":"Paypal - Ratepay GmbH - Test Bank Account"'
            . '}}',
            (string)$paymentSource
        );
    }
}
