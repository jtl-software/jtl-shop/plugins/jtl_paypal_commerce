<?php

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment;

use JsonException;
use PHPUnit\Framework\TestCase;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\CardPaymentSource;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\PaymentSourceBuilder;
use Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment\TestSources\PayPalPaymentData;

class CardPaymentSourceTest extends TestCase
{
    private object $data;

    protected function setUp(): void
    {
        parent::setUp();

        $attributes        = (object)[
            'customer'     => (object)[
                'id'            => '123',
                'email_address' => 'test@example.com',
            ],
            'verification' => 'SCA_ALWAYS',
        ];
        $billingAddress    = (object)[
            'country_code'   => 'DE',
            'address_line_1' => 'Große Ullrichstr. 49',
            'address_line_2' => 'über Spiegelstr.',
            'admin_area_2'   => 'Halle',
            'postal_code'    => '06108',
        ];
        $experienceContext = (object)[
            'return_url' => 'https://example.com/return',
            'cancel_url' => 'https://example.com/cancel',
        ];

        $this->data = (object)[
            'name'               => 'Tom Tester',
            'number'             => '1234 5678 1562 1458',
            'security_code'      => '1234',
            'expiry'             => '2024-09',
            'billing_address'    => $billingAddress,
            'attributes'         => $attributes,
            'stored_credential'  => (object)[
                'payment_initiator' => 'MERCHANT',
                'payment_type'      => 'RECURRING',
                'usage'             => 'SUBSEQUENT',
            ],
            'vault_id'           => 'xyzHGFD6345',
            'experience_context' => $experienceContext,
        ];
    }

    public function testInstanceCreation(): void
    {
        $paymentSource = new CardPaymentSource($this->data);
        $this->assertInstanceOf(CardPaymentSource::class, $paymentSource);
    }

    /**
     * @throws JsonException
     */
    public function testJsonSerialize(): void
    {
        $data          = clone $this->data;
        $paymentSource = new CardPaymentSource(clone $data);
        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );

        $paymentSource->setName('Paul Büchner');
        $data->name = 'Paul Büchner';
        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );

        $paymentSource->setNumber('1234 1562 5678 1458');
        $data->number = '1234 1562 5678 1458';
        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );

        $paymentSource->setSecurityCode('432');
        $data->security_code = '432';
        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );

        $paymentSource->setExpiry('2027-09');
        $data->expiry = '2027-09';
        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );

        $paymentSource->setExperienceContext();
        unset($data->experience_context);
        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );

        $paymentSource->setStoredCredential();
        unset($data->stored_credential);
        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );

        $paymentSource->setAttributes();
        unset($data->attributes);
        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );

        $paymentSource->setBillingAddress();
        unset($data->billing_address);
        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );
    }

    /**
     * @throws JsonException
     */
    public function testInvalidJsonSerialize(): void
    {
        $data                = clone $this->data;
        $data->email_address = 'test@example.com';
        $paymentSource       = new CardPaymentSource($data);
        $this->assertEquals(
            \json_encode($this->data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );
    }

    /**
     * @throws JsonException
     */
    public function testCreation(): void
    {
        $paymentData   = new PayPalPaymentData(PaymentSourceBuilder::FUNDING_CARD);
        $paymentSource = $paymentData->build();
        $payer         = $paymentData->createPayer();
        $address       = $payer->getAddress();
        $expectedJSON  = \sprintf(
            '{
                "name":"%s",
                "billing_address":{
                    "address_line_1":"%s",
                    "address_line_2":"%s",
                    "country_code":"%s",
                    "postal_code":"%s",
                    "admin_area_2":"%s"
                }
            }',
            $payer->getGivenName() . ' ' . $payer->getSurname(),
            $address->getAddress()[0],
            $address->getAddress()[1],
            $address->getCountryCode(),
            $address->getPostalCode(),
            $address->getCity()
        );

        $this->assertEquals(
            \json_encode(
                \json_decode($expectedJSON, null, 512, JSON_THROW_ON_ERROR),
                JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES
            ),
            (string)$paymentSource
        );
    }

    public function testAttributeVault(): void
    {
        $paymentSource = (new CardPaymentSource())->withVaultRequest();
        $this->assertEquals(
            '{'
            . '"attributes":{'
                . '"vault":{'
                    . '"store_in_vault":"ON_SUCCESS"'
                . '}'
            . '},'
            . '"stored_credential":{'
                . '"payment_initiator":"CUSTOMER",'
                . '"payment_type":"ONE_TIME",'
                . '"usage":"FIRST"'
            . '}'
            . '}',
            (string)$paymentSource
        );
        $paymentSource->withVaultRequest(false);
        $this->assertEquals('{}', (string)$paymentSource);
    }
}
