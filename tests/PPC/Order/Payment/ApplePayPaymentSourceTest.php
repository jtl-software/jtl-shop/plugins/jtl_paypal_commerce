<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment;

use JsonException;
use PHPUnit\Framework\TestCase;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\ApplePayPaymentSource;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\PaymentSourceBuilder;
use Plugin\jtl_paypal_commerce\PPC\Request\Serializer\JSON;
use Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment\TestSources\ApplePaymentData;

/**
 * Class ApplePayPaymentSourceTest
 * @package PPC\Order\Payment
 */
class ApplePayPaymentSourceTest extends TestCase
{
    private object $data;

    protected function setUp(): void
    {
        parent::setUp();

        $attributes = (object)[
            'customer' => (object)[
                'id'            => '1234ABC-xyz',
                'email_address' => 'merchant@example.com',
                'phone'         => (object)[
                    'phone_number' => (object)[
                        'national_number' => '0049987654321',
                    ],
                ],
            ],
            'vault'    => (object)[
                'store_in_vault' => 'ON_SUCCESS',
            ],
        ];
        $this->data = (object)[
            'id'                => 'ABC1234XYZ',
            'stored_credential' => (object)[
                'payment_initiator' => 'MERCHANT',
                'payment_type'      => 'RECURRING',
                'usage'             => 'SUBSEQUENT',
            ],
            'attributes'        => $attributes,
            'name'              => 'Tom Tester',
            'email_address'     => 'tom.tester@example.com',
            'phone_number'      => (object)[
                'national_number' => '0049123456789',
            ],
            'vault_id'          => 'XYZ_vgf-123',
        ];
    }

    public function testInstanceCreation(): void
    {
        $paymentSource = new ApplePayPaymentSource($this->data);
        $this->assertInstanceOf(ApplePayPaymentSource::class, $paymentSource);
    }

    /**
     * @throws JsonException
     */
    public function testJsonSerialize(): void
    {
        $data          = clone $this->data;
        $paymentSource = new ApplePayPaymentSource(clone $data);
        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );

        $paymentSource->setId('1234XYZABC');
        $data->id = '1234XYZABC';
        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );

        $paymentSource->setVaultId('456XYZ_vgf-123');
        $data->vault_id = '456XYZ_vgf-123';
        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );

        $paymentSource->setStoredCredential();
        unset($data->stored_credential);
        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );
    }

    /**
     * @throws JsonException
     */
    public function testSetGetName(): void
    {
        $data          = clone $this->data;
        $paymentSource = new ApplePayPaymentSource(clone $data);
        $this->assertEquals('Tom Tester', $paymentSource->getName());
        $paymentSource->setName('Hans Großmann');
        $this->assertEquals('Hans Großmann', $paymentSource->getName());
        $data->name = 'Hans Großmann';
        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );
    }

    /**
     * @throws JsonException
     */
    public function testSetGetEmail(): void
    {
        $data          = clone $this->data;
        $paymentSource = new ApplePayPaymentSource(clone $data);
        $this->assertEquals('tom.tester@example.com', $paymentSource->getEmail());
        $paymentSource->setEmail('karla.columna@kolumne.com');
        $this->assertEquals('karla.columna@kolumne.com', $paymentSource->getEmail());
        $data->email_address = 'karla.columna@kolumne.com';
        $this->assertEquals('karla.columna@kolumne.com', $paymentSource->fetchPayer()->getEmail());
        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );
    }

    /**
     * @throws JsonException
     */
    public function testSetGetPhone(): void
    {
        $data          = clone $this->data;
        $paymentSource = new ApplePayPaymentSource(clone $data);
        $payer         = $paymentSource->fetchPayer();
        $this->assertEquals('0049123456789', $payer->getPhone()->getNumber('00', ''));
        $phone = $payer->getPhone();
        $payer->setPhone($phone->setNumber('004934589765432'));
        $this->assertEquals('004934589765432', $payer->getPhone()->getNumber('00', ''));
        $paymentSource->setPhoneNumber($phone);
        $data->phone_number = (object)[
            'national_number' => '004934589765432',
        ];
        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );
    }

    public function testSetGetAttributes(): void
    {
        $paymentSource = new ApplePayPaymentSource(clone $this->data);
        $this->assertEquals('1234ABC-xyz', $paymentSource->getAttribute('customer')->getData()->id);
        $this->assertEquals(
            '0049987654321',
            $paymentSource->getAttribute('customer')->getData()->phone->phone_number->national_number
        );
        $vault = $paymentSource->getAttribute('vault')->getData();
        $this->assertEquals('ON_SUCCESS', $vault->store_in_vault);
        $vault->store_in_vault = 'ON_FAILURE';
        $paymentSource->addAttribute('vault', new JSON($vault));
        $this->assertEquals(
            '{'
                . '"customer":{'
                    . '"id":"1234ABC-xyz",'
                    . '"email_address":"merchant@example.com",'
                    . '"phone":{'
                        . '"phone_number":{'
                            . '"national_number":"0049987654321"'
                        . '}'
                    . '}'
                . '},'
                . '"vault":{'
                    . '"store_in_vault":"ON_FAILURE"'
                . '}'
            . '}',
            (string)$paymentSource->getAttributes()
        );
    }

    /**
     * @throws JsonException
     */
    public function testCreation(): void
    {
        $paymentData   = new ApplePaymentData(PaymentSourceBuilder::FUNDING_APPLEPAY);
        $paymentSource = $paymentData->build();
        $payer         = $paymentData->createPayer();
        $expectedJSON  = \sprintf(
            '{
                "name":"%s",
                "email_address":"%s",
                "phone_number":{"national_number":"%s"}
            }',
            $payer->getGivenName() . ' ' . $payer->getSurname(),
            $payer->getEmail(),
            $payer->getPhone()->getNumber('00', '')
        );

        $this->assertEquals(
            \json_encode(
                \json_decode($expectedJSON, null, 512, JSON_THROW_ON_ERROR),
                JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES
            ),
            (string)$paymentSource
        );
    }
}
