<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment\TestSources\PropertyCountryCodePaymentSource;

/**
 * Class PropertyCountryCodeTest
 * @package PPC\Order\Payment
 */
class PropertyCountryCodeTest extends TestCase
{
    public function testSetGetCountryCode(): void
    {
        $paymentSource = new PropertyCountryCodePaymentSource((object)['country_code' => 'DE']);
        $this->assertEquals('DE', $paymentSource->getCountryCode());
        $paymentSource->setCountryCode('EN');
        $this->assertEquals('EN', $paymentSource->getCountryCode());
    }

    public function testSetCountryCodeInValidNumber(): void
    {
        $paymentSource = new PropertyCountryCodePaymentSource((object)['country_code' => 'DE']);
        $this->expectException(InvalidArgumentException::class);
        $paymentSource->setCountryCode('11');
    }

    public function testSetCountryCodeInValidLong(): void
    {
        $paymentSource = new PropertyCountryCodePaymentSource((object)['country_code' => 'DE']);
        $this->expectException(InvalidArgumentException::class);
        $paymentSource->setCountryCode('DEE');
    }

    public function testSetCountryCodeInValidShort(): void
    {
        $paymentSource = new PropertyCountryCodePaymentSource((object)['country_code' => 'DE']);
        $this->expectException(InvalidArgumentException::class);
        $paymentSource->setCountryCode('DEE');
    }
}
