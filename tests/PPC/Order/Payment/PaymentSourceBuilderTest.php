<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment;

use PHPUnit\Framework\TestCase;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\ApplePayPaymentSource;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\BancontactPaymentSource;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\BlikPaymentSource;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\CardPaymentSource;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\EpsPaymentSource;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\GiropayPaymentSource;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\GooglePayPaymentSource;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\IDealPaymentSource;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\MyBankPaymentSource;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\P24PaymentSource;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\PaymentSourceBuilder;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\PayPalPaymentSource;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\SofortPaymentSource;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\TokenPaymentSource;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\TrustlyPaymentSource;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\VenmoPaymentSource;

/**
 * Class PaymentSourceBuildertest
 * @package PPC\Order\Payment
 */
class PaymentSourceBuilderTest extends TestCase
{
    public function testBuildCard(): void
    {
        $paymentSourceBuilder = new PaymentSourceBuilder(PaymentSourceBuilder::FUNDING_CARD);
        $paymentSource        = $paymentSourceBuilder->setData((object)[
            'name'   => 'Tom Tester',
            'number' => '1234 5678 3456 0987',
        ])->build();
        $this->assertInstanceOf(CardPaymentSource::class, $paymentSource);
        $this->assertEquals('Tom Tester', $paymentSource->getName());
        $this->assertEquals('1234 5678 3456 0987', $paymentSource->getNumber());
    }

    public function testBuildToken(): void
    {
        $paymentSourceBuilder = new PaymentSourceBuilder(PaymentSourceBuilder::FUNDING_TOKEN);
        $paymentSource        = $paymentSourceBuilder->setData((object)[
            'id'   => 'ABC-2345',
            'type' => 'BILLING_AGREEMENT',
        ])->build();
        $this->assertInstanceOf(TokenPaymentSource::class, $paymentSource);
        $this->assertEquals('ABC-2345', $paymentSource->getId());
        $this->assertEquals('BILLING_AGREEMENT', $paymentSource->getType());
    }

    public function testBuildPayPal(): void
    {
        $paymentSourceBuilder = new PaymentSourceBuilder(PaymentSourceBuilder::FUNDING_PAYPAL);
        $paymentSource        = $paymentSourceBuilder->setData((object)[
            'email_address' => 'tom.tester@testmail.de',
            'vault_id'      => 'ABC-2345',
        ])->build();
        $this->assertInstanceOf(PayPalPaymentSource::class, $paymentSource);
        $this->assertEquals('tom.tester@testmail.de', $paymentSource->getEmail());
        $this->assertEquals('ABC-2345', $paymentSource->getVaultId());
    }

    public function testBuildBancontact(): void
    {
        $paymentSourceBuilder = new PaymentSourceBuilder(PaymentSourceBuilder::FUNDING_BANCONTACT);
        $paymentSource        = $paymentSourceBuilder->setData((object)[
            'name'         => 'Tom Tester',
            'country_code' => 'EN',
        ])->build();
        $this->assertInstanceOf(BancontactPaymentSource::class, $paymentSource);
        $this->assertEquals('Tom Tester', $paymentSource->getName());
        $this->assertEquals('EN', $paymentSource->getCountryCode());
    }

    public function testBuildBlik(): void
    {
        $paymentSourceBuilder = new PaymentSourceBuilder(PaymentSourceBuilder::FUNDING_BLIK);
        $paymentSource        = $paymentSourceBuilder->setData((object)[
            'name'         => 'Tom Tester',
            'country_code' => 'EN',
        ])->build();
        $this->assertInstanceOf(BlikPaymentSource::class, $paymentSource);
        $this->assertEquals('Tom Tester', $paymentSource->getName());
        $this->assertEquals('EN', $paymentSource->getCountryCode());
    }

    public function testBuildEps(): void
    {
        $paymentSourceBuilder = new PaymentSourceBuilder(PaymentSourceBuilder::FUNDING_EPS);
        $paymentSource        = $paymentSourceBuilder->setData((object)[
            'name'         => 'Tom Tester',
            'country_code' => 'EN',
        ])->build();
        $this->assertInstanceOf(EpsPaymentSource::class, $paymentSource);
        $this->assertEquals('Tom Tester', $paymentSource->getName());
        $this->assertEquals('EN', $paymentSource->getCountryCode());
    }

    public function testBuildGiropay(): void
    {
        $paymentSourceBuilder = new PaymentSourceBuilder(PaymentSourceBuilder::FUNDING_GIROPAY);
        $paymentSource        = $paymentSourceBuilder->setData((object)[
            'name'         => 'Tom Tester',
            'country_code' => 'EN',
        ])->build();
        $this->assertInstanceOf(GiropayPaymentSource::class, $paymentSource);
        $this->assertEquals('Tom Tester', $paymentSource->getName());
        $this->assertEquals('EN', $paymentSource->getCountryCode());
    }

    public function testBuildIDeal(): void
    {
        $paymentSourceBuilder = new PaymentSourceBuilder(PaymentSourceBuilder::FUNDING_IDEAL);
        $paymentSource        = $paymentSourceBuilder->setData((object)[
            'name'         => 'Tom Tester',
            'country_code' => 'EN',
        ])->build();
        $this->assertInstanceOf(IDealPaymentSource::class, $paymentSource);
        $this->assertEquals('Tom Tester', $paymentSource->getName());
        $this->assertEquals('EN', $paymentSource->getCountryCode());
    }

    public function testBuildMyBank(): void
    {
        $paymentSourceBuilder = new PaymentSourceBuilder(PaymentSourceBuilder::FUNDING_MYBANK);
        $paymentSource        = $paymentSourceBuilder->setData((object)[
            'name'         => 'Tom Tester',
            'country_code' => 'EN',
        ])->build();
        $this->assertInstanceOf(MyBankPaymentSource::class, $paymentSource);
        $this->assertEquals('Tom Tester', $paymentSource->getName());
        $this->assertEquals('EN', $paymentSource->getCountryCode());
    }

    public function testBuildP24(): void
    {
        $paymentSourceBuilder = new PaymentSourceBuilder(PaymentSourceBuilder::FUNDING_P24);
        $paymentSource        = $paymentSourceBuilder->setData((object)[
            'name'         => 'Tom Tester',
            'country_code' => 'EN',
        ])->build();
        $this->assertInstanceOf(P24PaymentSource::class, $paymentSource);
        $this->assertEquals('Tom Tester', $paymentSource->getName());
        $this->assertEquals('EN', $paymentSource->getCountryCode());
    }

    public function testBuildSofort(): void
    {
        $paymentSourceBuilder = new PaymentSourceBuilder(PaymentSourceBuilder::FUNDING_SOFORT);
        $paymentSource        = $paymentSourceBuilder->setData((object)[
            'name'         => 'Tom Tester',
            'country_code' => 'EN',
        ])->build();
        $this->assertInstanceOf(SofortPaymentSource::class, $paymentSource);
        $this->assertEquals('Tom Tester', $paymentSource->getName());
        $this->assertEquals('EN', $paymentSource->getCountryCode());
    }

    public function testBuildTrustly(): void
    {
        $paymentSourceBuilder = new PaymentSourceBuilder(PaymentSourceBuilder::FUNDING_TRUSTLY);
        $paymentSource        = $paymentSourceBuilder->setData((object)[
            'name'         => 'Tom Tester',
            'country_code' => 'EN',
        ])->build();
        $this->assertInstanceOf(TrustlyPaymentSource::class, $paymentSource);
        $this->assertEquals('Tom Tester', $paymentSource->getName());
        $this->assertEquals('EN', $paymentSource->getCountryCode());
    }

    public function testBuildApplePay(): void
    {
        $paymentSourceBuilder = new PaymentSourceBuilder(PaymentSourceBuilder::FUNDING_APPLEPAY);
        $paymentSource        = $paymentSourceBuilder->setData((object)[
            'name' => 'Tom Tester',
            'id'   => '1234-5678-3456-0987',
        ])->build();
        $this->assertInstanceOf(ApplePayPaymentSource::class, $paymentSource);
        $this->assertEquals('Tom Tester', $paymentSource->getName());
        $this->assertEquals('1234-5678-3456-0987', $paymentSource->getId());
    }

    public function testBuildGooglePay(): void
    {
        $paymentSourceBuilder = new PaymentSourceBuilder(PaymentSourceBuilder::FUNDING_GOOGLEPAY);
        $paymentSource        = $paymentSourceBuilder->setData((object)[
            'name'          => 'Tom Tester',
            'email_address' => 'tom.tester@testmail.de',
        ])->build();
        $this->assertInstanceOf(GooglePayPaymentSource::class, $paymentSource);
        $this->assertEquals('Tom Tester', $paymentSource->getName());
        $this->assertEquals('tom.tester@testmail.de', $paymentSource->getEmail());
    }

    public function testBuildVenmo(): void
    {
        $paymentSourceBuilder = new PaymentSourceBuilder(PaymentSourceBuilder::FUNDING_VENMO);
        $paymentSource        = $paymentSourceBuilder->setData((object)[
            'email_address' => 'tom.tester@testmail.de',
            'vault_id'      => 'ABC-2345',
        ])->build();
        $this->assertInstanceOf(VenmoPaymentSource::class, $paymentSource);
        $this->assertEquals('tom.tester@testmail.de', $paymentSource->getEmail());
        $this->assertEquals('ABC-2345', $paymentSource->getVaultId());
    }
}
