<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment;

use PHPUnit\Framework\TestCase;
use Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment\TestSources\PropertyNamePaymentSource;

class PropertyNameTest extends TestCase
{
    public function testSetGetName(): void
    {
        $paymentSource = new PropertyNamePaymentSource((object)['name' => 'Toni Turbo']);
        $this->assertEquals('Toni Turbo', $paymentSource->getName());

        $instance = $paymentSource->setName('Tom Tester');
        $this->assertInstanceOf(PropertyNamePaymentSource::class, $instance);
        $this->assertEquals('Tom Tester', $paymentSource->getName());
        $this->assertEquals('{"name":"Tom Tester"}', (string)$paymentSource);
    }

    public function testShortenName(): void
    {
        $paymentSource = new PropertyNamePaymentSource();
        $name          = str_pad('1234', 320);
        $paymentSource->setName($name);
        $this->assertEquals(300, strlen($paymentSource->getName()));
    }
}
