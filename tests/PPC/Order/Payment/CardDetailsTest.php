<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment;

use InvalidArgumentException;
use Plugin\jtl_paypal_commerce\PPC\Order\Address;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\AuthResult;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\CardDetails;
use PHPUnit\Framework\TestCase;

class CardDetailsTest extends TestCase
{
    private string $data;

    /**
     * @inheritDoc
     * @throws \JsonException
     */
    protected function setUp(): void
    {
        $data = (object)[
            'name'            => 'Tom Tester',
            'type'            => 'CREDIT',
            'brand'           => 'MASTERCARD',
            'last_digits'     => '1234',
            'authentication_result' => (object)[
                'liability_shift' => 'POSSIBLE'
            ],
            'billing_address' => (object)[
                'address_line_1' => 'Große Ullrichstr. 49',
                'address_line_2' => 'Eingang über Spiegelstr.',
                'admin_area_1'   => 'Sachsen-Anhalt',
                'admin_area_2'   => 'Halle',
                'postal_code'    => '06108',
                'country_code'   => 'DE',
            ],
        ];

        $this->data = \json_encode($data, \JSON_THROW_ON_ERROR | \JSON_UNESCAPED_UNICODE);
    }

    /**
     * @throws \JsonException
     */
    public function testInstanceCreation(): CardDetails
    {
        $cardDetails = new CardDetails(\json_decode($this->data, false, 512, \JSON_THROW_ON_ERROR));
        $this->assertInstanceOf(CardDetails::class, $cardDetails);
        $this->assertEquals($this->data, (string)$cardDetails);

        return $cardDetails;
    }

    /**
     * @depends testInstanceCreation
     * @param CardDetails $cardDetails
     * @return void
     */
    public function testSetGetName(CardDetails $cardDetails): void
    {
        $this->assertEquals('Tom Tester', $cardDetails->getName());
        $cardDetails->setName('Toni Turbo');
        $this->assertEquals('Toni Turbo', $cardDetails->getName());
        $cardDetails->setName();
        $this->assertEquals('', $cardDetails->getName());

        $this->expectException(InvalidArgumentException::class);
        $cardDetails->setName('');
    }

    /**
     * @depends testInstanceCreation
     * @param CardDetails $cardDetails
     * @return void
     */
    public function testSetGetBillingAddress(CardDetails $cardDetails): void
    {
        $address = $cardDetails->getBillingAddress();
        $this->assertInstanceOf(Address::class, $address);
        $this->assertEquals('Große Ullrichstr. 49', $address->getAddress()[0]);
        $address = new Address();
        $address->setAddress(['Kleine Ullrichstr. 87']);
        $cardDetails->setBillingAddress($address);
        $this->assertEquals('Kleine Ullrichstr. 87', $cardDetails->getBillingAddress()->getAddress()[0]);
    }

    /**
     * @depends testInstanceCreation
     * @param CardDetails $cardDetails
     * @return void
     */
    public function testSetGetType(CardDetails $cardDetails): void
    {
        $this->assertEquals('CREDIT', $cardDetails->getType());
        $cardDetails->setType('DEBIT');
        $this->assertEquals('DEBIT', $cardDetails->getType());
        $cardDetails->setType();
        $this->assertEquals('', $cardDetails->getType());

        $this->expectException(InvalidArgumentException::class);
        $cardDetails->setType('keine!');
    }

    /**
     * @depends testInstanceCreation
     * @param CardDetails $cardDetails
     * @return void
     */
    public function testSetGetBrand(CardDetails $cardDetails): void
    {
        $this->assertEquals('MASTERCARD', $cardDetails->getBrand());
        $cardDetails->setBrand('AMEX');
        $this->assertEquals('AMEX', $cardDetails->getBrand());
        $cardDetails->setBrand();
        $this->assertEquals('', $cardDetails->getBrand());

        $this->expectException(InvalidArgumentException::class);
        $cardDetails->setBrand('keine!');
    }

    /**
     * @depends testInstanceCreation
     * @param CardDetails $cardDetails
     * @return void
     */
    public function testSetGetLastDigits(CardDetails $cardDetails): void
    {
        $this->assertEquals('1234', $cardDetails->getLastDigits());
        $cardDetails->setLastDigits('789');
        $this->assertEquals('789', $cardDetails->getLastDigits());
        $cardDetails->setLastDigits();
        $this->assertEquals('', $cardDetails->getLastDigits());
    }

    /**
     * @depends testInstanceCreation
     * @param CardDetails $cardDetails
     * @return void
     */
    public function testSetGetAuthResult(CardDetails $cardDetails): void
    {
        $authResult = $cardDetails->getAuthResult();
        $this->assertInstanceOf(AuthResult::class, $authResult);
        $this->assertEquals('POSSIBLE', $authResult->getLiabilityShift());

        $authResult = (new AuthResult())->setLiabilityShift(AuthResult::LB_NO);
        $cardDetails->setAuthResult($authResult);
        $this->assertEquals('NO', $authResult->getLiabilityShift());
    }

    /**
     * @throws \JsonException
     */
    public function testJsonSerialize(): void
    {
        $cardDetails = new CardDetails(\json_decode($this->data, false, 512, \JSON_THROW_ON_ERROR));
        $this->assertEquals($this->data, (string)$cardDetails);
        $cardDetails->setAuthResult();
        $this->assertEquals(
            '{'
            . '"name":"Tom Tester",'
            . '"type":"CREDIT",'
            . '"brand":"MASTERCARD",'
            . '"last_digits":"1234",'
            . '"billing_address":{'
                . '"address_line_1":"Große Ullrichstr. 49",'
                . '"address_line_2":"Eingang über Spiegelstr.",'
                . '"admin_area_1":"Sachsen-Anhalt",'
                . '"admin_area_2":"Halle",'
                . '"postal_code":"06108",'
                . '"country_code":"DE"'
            . '}}',
            (string)$cardDetails
        );
        $cardDetails->setBillingAddress();
        /** @noinspection PhpConditionAlreadyCheckedInspection */
        $this->assertEquals(
            '{'
            . '"name":"Tom Tester",'
            . '"type":"CREDIT",'
            . '"brand":"MASTERCARD",'
            . '"last_digits":"1234"'
            . '}',
            (string)$cardDetails
        );
        $cardDetails->setLastDigits();
        /** @noinspection PhpConditionAlreadyCheckedInspection */
        $this->assertEquals(
            '{'
            . '"name":"Tom Tester",'
            . '"type":"CREDIT",'
            . '"brand":"MASTERCARD"'
            . '}',
            (string)$cardDetails
        );
        $cardDetails->setBrand();
        /** @noinspection PhpConditionAlreadyCheckedInspection */
        $this->assertEquals(
            '{'
            . '"name":"Tom Tester",'
            . '"type":"CREDIT"'
            . '}',
            (string)$cardDetails
        );
        $cardDetails->setType();
        /** @noinspection PhpConditionAlreadyCheckedInspection */
        $this->assertEquals(
            '{'
            . '"name":"Tom Tester"'
            . '}',
            (string)$cardDetails
        );
    }
}
