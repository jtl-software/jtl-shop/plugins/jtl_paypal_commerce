<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment;

use PHPUnit\Framework\TestCase;
use Plugin\jtl_paypal_commerce\PPC\Order\Phone;
use Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment\TestSources\PropertyPhonePaymentSource;

/**
 * Class PropertyPhonePaymentSourceTest
 * @package PPC\Order\Payment
 */
class PropertyPhonePaymentSourceTest extends TestCase
{
    public function testSetGetPhoneNumber(): void
    {
        $paymentSource = new PropertyPhonePaymentSource((object)[
            'phone_number' => (object)[
                'national_number' => '0049123456789',
            ]
        ]);
        $phone         = $paymentSource->getPhoneNumber();
        $this->assertInstanceOf(Phone::class, $phone);
        $this->assertEquals('0049123456789', $phone->getNumber('00', ''));
        $this->assertEquals('{"phone_number":{"national_number":"0049123456789"}}', (string)$paymentSource);
        $phone->setNationalNumber('989745612')->setCountryCode('42');
        $paymentSource->setPhoneNumber($phone);
        $this->assertEquals('0042989745612', $paymentSource->getPhoneNumber()->getNumber('00', ''));
        $this->assertEquals('{"phone_number":{"national_number":"0042989745612"}}', (string)$paymentSource);
    }

    public function testInvalidPhoneNumber(): void
    {
        $paymentSource = new PropertyPhonePaymentSource();
        $this->assertNull($paymentSource->getPhoneNumber());
        $paymentSource = new PropertyPhonePaymentSource((object)['phone_number' => '']);
        $this->assertNull($paymentSource->getPhoneNumber());
        $this->assertEquals('{}', (string)$paymentSource);
    }
}
