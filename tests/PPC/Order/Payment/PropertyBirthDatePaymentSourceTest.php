<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment;

use DateTime;
use PHPUnit\Framework\TestCase;
use Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment\TestSources\PropertyBirthDatePaymentSource;

/**
 * Class PropertyBirthDatePaymentSourceTest
 * @package PPC\Order\Payment
 */
class PropertyBirthDatePaymentSourceTest extends TestCase
{
    public function testSetGetBirthDate(): void
    {
        $paymentSource = new PropertyBirthDatePaymentSource((object)['birth_date' => '1999-01-01']);
        $birthDate     = $paymentSource->getBirthDate();
        $this->assertInstanceOf(DateTime::class, $birthDate);
        $this->assertEquals('1999-01-01', $birthDate->format('Y-m-d'));
        $this->assertEquals('{"birth_date":"1999-01-01"}', (string)$paymentSource);
        $birthDate->setDate(2001, 10, 15);
        $paymentSource->setBirthDate($birthDate);
        $this->assertEquals('2001-10-15', $paymentSource->getBirthDate()->format('Y-m-d'));
        $this->assertEquals('{"birth_date":"2001-10-15"}', (string)$paymentSource);
    }

    public function testInvalidBirthDate(): void
    {
        $paymentSource = new PropertyBirthDatePaymentSource();
        $this->assertNull($paymentSource->getBirthDate());
        $paymentSource = new PropertyBirthDatePaymentSource((object)['birth_date' => 'ABC']);
        $this->assertNull($paymentSource->getBirthDate());
        $this->assertEquals('{}', (string)$paymentSource);
    }
}
