<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment;

use PHPUnit\Framework\TestCase;
use Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment\TestSources\PropertyBicPaymentSource;

/**
 * Class PropertyBicPaymentSourceTest
 * @package PPC\Order\Payment
 */
class PropertyBicPaymentSourceTest extends TestCase
{
    public function testSetGetBIC(): void
    {
        $paymentSource = new PropertyBicPaymentSource((object)['bic' => 'DEUTDEFFXXX']);
        $this->assertEquals('DEUTDEFFXXX', $paymentSource->getBIC());
        $paymentSource->setBIC('COBADEFFXXX');
        $this->assertEquals('COBADEFFXXX', $paymentSource->getBIC());
    }

    public function testSetBICFailInvalidLong(): void
    {
        $paymentSource = new PropertyBicPaymentSource((object)['bic' => 'DEUTDEFFXXX']);
        $this->expectException(\InvalidArgumentException::class);
        $paymentSource->setBIC('CO-COBADEFFXXXY');
    }

    public function testSetBICFailInvalidShort(): void
    {
        $paymentSource = new PropertyBicPaymentSource((object)['bic' => 'DEUTDEFFXXX']);
        $this->expectException(\InvalidArgumentException::class);
        $paymentSource->setBIC('COBADEFFXY');
    }

    public function testSetBICFailInvalid(): void
    {
        $paymentSource = new PropertyBicPaymentSource((object)['bic' => 'DEUTDEFFXXX']);
        $this->expectException(\InvalidArgumentException::class);
        $paymentSource->setBIC('COBADEF1XX');
    }
}
