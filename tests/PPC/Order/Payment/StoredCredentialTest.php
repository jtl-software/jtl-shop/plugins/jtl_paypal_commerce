<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment;

use InvalidArgumentException;
use JsonException;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\NetworkTransactionReference;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\StoredCredential;
use PHPUnit\Framework\TestCase;
use stdClass;

class StoredCredentialTest extends TestCase
{
    private string $data;

    /**
     * @throws JsonException
     */
    protected function setUp(): void
    {
        parent::setUp();

        $networkRef = new NetworkTransactionReference((object)['id' => 'abcKL234#']);
        $networkRef->setNetwork('VISA');
        $networkRef->setRefNumber('123456789');

        $data                                         = new stdClass();
        $data->payment_initiator                      = StoredCredential::PI_CUSTOMER;
        $data->payment_type                           = StoredCredential::PTYPE_ONE_TIME;
        $data->previous_network_transaction_reference = $networkRef;

        $this->data = \json_encode($data, \JSON_THROW_ON_ERROR | \JSON_UNESCAPED_UNICODE);
    }

    /**
     * @throws JsonException
     */
    public function testInstanceCreation(): StoredCredential
    {
        $storedCredential = new StoredCredential(\json_decode($this->data, false, 512, \JSON_THROW_ON_ERROR));
        $this->assertInstanceOf(StoredCredential::class, $storedCredential);
        $this->assertEquals($this->data, (string)$storedCredential);

        return $storedCredential;
    }

    /**
     * @depends testInstanceCreation
     * @param StoredCredential $credentials
     * @return void
     */
    public function testSetGetPaymentInitiator(StoredCredential $credentials): void
    {
        $credentials->setPaymentType(StoredCredential::PTYPE_RECURRING);
        $credentials->setPaymentInitiator(StoredCredential::PI_MERCHANT);
        $this->assertEquals(StoredCredential::PI_MERCHANT, $credentials->getPaymentInitiator());
        $this->assertEquals(
            StoredCredential::PI_CUSTOMER,
            $credentials->setPaymentInitiator(StoredCredential::PI_CUSTOMER)->getPaymentInitiator()
        );
        $credentials->setPaymentType(StoredCredential::PTYPE_ONE_TIME);

        $this->expectException(InvalidArgumentException::class);
        $credentials->setPaymentInitiator(StoredCredential::PI_MERCHANT);
    }

    /**
     * @depends testInstanceCreation
     * @param StoredCredential $credentials
     * @return void
     */
    public function testSetGetPaymentType(StoredCredential $credentials): void
    {
        $credentials->setPaymentType(StoredCredential::PTYPE_RECURRING);
        $this->assertEquals(StoredCredential::PTYPE_RECURRING, $credentials->getPaymentType());
        $credentials->setPaymentInitiator(StoredCredential::PI_MERCHANT);

        $this->expectException(InvalidArgumentException::class);
        $credentials->setPaymentType(StoredCredential::PTYPE_ONE_TIME);
    }

    /**
     * @depends testInstanceCreation
     * @param StoredCredential $credentials
     * @return void
     */
    public function testSetGetUsage(StoredCredential $credentials): void
    {
        $credentials->setUsage(StoredCredential::USAGE_DERIVED);
        $this->assertEquals(StoredCredential::USAGE_DERIVED, $credentials->getUsage());
        $credentials->setPaymentInitiator(StoredCredential::PI_MERCHANT);

        $this->expectException(InvalidArgumentException::class);
        $credentials->setUsage(StoredCredential::USAGE_FIRST);
    }

    /**
     * @depends testInstanceCreation
     * @param StoredCredential $credentials
     * @return void
     */
    public function testSetGetPreviousNetworkTransactionReference(StoredCredential $credentials): void
    {
        $this->assertInstanceOf(
            NetworkTransactionReference::class,
            $credentials->getPreviousNetworkTransactionReference()
        );
        $credentials->setPreviousNetworkTransactionReference(
            new NetworkTransactionReference((object)['id' => 'abcKL#999'])
        );
        $this->assertEquals('abcKL#999', $credentials->getPreviousNetworkTransactionReference()->getId());
    }

    /**
     * @throws JsonException
     */
    public function testJsonSerialize(): void
    {
        $credentials = new StoredCredential(\json_decode($this->data, false, 512, \JSON_THROW_ON_ERROR));
        $this->assertEquals($this->data, (string)$credentials);
    }
}
