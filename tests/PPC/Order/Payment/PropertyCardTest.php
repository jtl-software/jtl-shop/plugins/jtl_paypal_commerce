<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment\TestSources\PropertyCardPaymentSource;

class PropertyCardTest extends TestCase
{
    public function testSetGetNumber(): void
    {
        $paymentSource = new PropertyCardPaymentSource((object)['number' => '1234 5678 1562 1458']);
        $this->assertEquals('1234 5678 1562 1458', $paymentSource->getNumber());
        $instance = $paymentSource->setNumber('1234 1562 5678 1458');
        $this->assertInstanceOf(PropertyCardPaymentSource::class, $instance);
        $this->assertEquals('1234 1562 5678 1458', $instance->getNumber());
    }

    public function testFailNumber(): void
    {
        $paymentSource = new PropertyCardPaymentSource();
        $this->expectException(InvalidArgumentException::class);
        $paymentSource->setNumber('124 568 148');
    }

    public function testSetNumberTooLarge(): void
    {
        $paymentSource = new PropertyCardPaymentSource();
        $this->expectException(InvalidArgumentException::class);
        $paymentSource->setNumber('1234 5d78 dd 15a2 1458');
    }

    public function testSetGetSecurityCode(): void
    {
        $paymentSource = new PropertyCardPaymentSource((object)['security_code' => '1234']);
        $this->assertEquals('1234', $paymentSource->getSecurityCode());
        $instance = $paymentSource->setSecurityCode('4321');
        $this->assertInstanceOf(PropertyCardPaymentSource::class, $instance);
        $this->assertEquals('4321', $instance->getSecurityCode());
    }

    public function testFailSecurityCode(): void
    {
        $paymentSource = new PropertyCardPaymentSource();
        $this->expectException(InvalidArgumentException::class);
        $paymentSource->setSecurityCode('432199');
    }

    public function testSetGetExpiry(): void
    {
        $paymentSource = new PropertyCardPaymentSource((object)['expiry' => '2024-09']);
        $this->assertEquals('2024-09', $paymentSource->getExpiry());
        $instance = $paymentSource->setExpiry('2025-09');
        $this->assertInstanceOf(PropertyCardPaymentSource::class, $instance);
        $this->assertEquals('2025-09', $instance->getExpiry());
    }

    public function testFailExpiry(): void
    {
        $paymentSource = new PropertyCardPaymentSource((object)['expiry' => '2024-09']);
        $this->expectException(InvalidArgumentException::class);
        $paymentSource->setExpiry('2024');
    }
}
