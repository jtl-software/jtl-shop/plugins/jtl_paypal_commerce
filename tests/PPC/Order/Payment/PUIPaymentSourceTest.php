<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment;

use JsonException;
use PHPUnit\Framework\TestCase;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\PaymentSourceBuilder;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\PUIPaymentSource;
use Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment\TestSources\PayPalPaymentData;

/**
 * Class PUIPaymentSourceTest
 * @package PPC\Order\Payment
 */
class PUIPaymentSourceTest extends TestCase
{
    private object $data;

    protected function setUp(): void
    {
        parent::setUp();

        $this->data = (object)[
            'birth_date'           => '1990-01-01',
            'name'                 => (object)[
                'given_name' => "John",
                'surname'    => 'Doe',
            ],
            'billing_address'      => (object)[
                'address_line_1' => 'Schönhauser Allee 84',
                'admin_area_2'   => 'Berlin',
                'postal_code'    => '10439',
                'country_code'   => 'DE',
            ],
            'email'                => 'buyer@example.com',
            'payment_reference'    => 'b8a1525dlYzu6Mn62umI',
            'deposit_bank_details' => (object)[
                'bic'                 => 'DEUTDEFFXXX',
                'bank_name'           => 'Deutsche Bank',
                'iban'                => 'DE89370400440532013000',
                'account_holder_name' => 'Paypal - Ratepay GmbH - Test Bank Account',
            ],
            'phone'                => (object)[
                'national_number' => '6912345678',
                'country_code'    => '49',
            ]];
    }

    public function testInstanceCreation(): void
    {
        $paymentSource = new PUIPaymentSource($this->data);
        $this->assertInstanceOf(PUIPaymentSource::class, $paymentSource);
    }

    public function testSetName(): void
    {
        $paymentSource = new PUIPaymentSource();
        $paymentSource->setName('Tom Tester');
        $this->assertEquals('{"name":{"given_name":"Tom","surname":"Tester"}}', (string)$paymentSource);
    }

    public function testRecreate(): void
    {
        $paymentSource = new PUIPaymentSource();
        $paymentSource->setName('Tom Tester');

        $recreatePaymentSource = new PUIPaymentSource($paymentSource->getData());
        $this->assertEquals('{"name":{"given_name":"Tom","surname":"Tester"}}', (string)$recreatePaymentSource);
    }

    public function testPhoneNumber(): void
    {
        $paymentSource = new PUIPaymentSource($this->data);
        $phoneNumber   = $paymentSource->getPhoneNumber();
        $this->assertEquals('+49 6912345678', $phoneNumber->getNumber());

        $paymentSource = new PUIPaymentSource();
        $phoneNumber->setNationalNumber('12345678');
        $paymentSource->setPhoneNumber($phoneNumber);
        $this->assertEquals('{"phone":{"national_number":"12345678","country_code":"49"}}', (string)$paymentSource);
    }

    /**
     * @throws JsonException
     */
    public function testJsonSerialize(): void
    {
        $data          = clone $this->data;
        $paymentSource = new PUIPaymentSource(clone $data);
        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );

        $data->name->given_name = 'Achim';
        $data->name->surname    = 'van Gröne';
        $paymentSource->setName('Achim van Gröne');
        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );

        $data->email = 'buyer_mail@example.com';
        $paymentSource->setEmail($data->email);
        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );
    }

    /**
     * @throws JsonException
     */
    public function testCreation(): void
    {
        $paymentData   = new PayPalPaymentData(PaymentSourceBuilder::FUNDING_PUI);
        $paymentSource = $paymentData->build();
        $experienceCtx = $paymentSource->getExperienceContext();
        $payer         = $paymentSource->fetchPayer();
        $address       = $payer->getAddress();
        $expectedJSON  = \sprintf(
            '{
                "name":{
                    "given_name":"%s",
                    "surname":"%s"
                },
                "email":"%s",
                "birth_date":"%s",
                "phone": {
                    "national_number": "%s",
                    "country_code": "%s"
                },
                "billing_address":{
                    "address_line_1":"%s",
                    "address_line_2":"%s",
                    "country_code":"%s",
                    "postal_code":"%s",
                    "admin_area_2":"%s"
                },
                "experience_context":{
                    "locale":"%s",
                    "brand_name":"%s",
                    "customer_service_instructions":[
                        "%s"
                    ]
                }
            }',
            $payer->getGivenName(),
            $payer->getSurname(),
            $payer->getEmail(),
            $payer->getBirthDate()->format('Y-m-d'),
            $payer->getPhone()->getNationalNumber(''),
            $payer->getPhone()->getCountryCode(''),
            $address->getAddress()[0],
            $address->getAddress()[1],
            $address->getCountryCode(),
            $address->getPostalCode(),
            $address->getCity(),
            $experienceCtx->getLocale(),
            $experienceCtx->getBrandName(),
            $experienceCtx->getCustomerServiceInstruction()
        );

        $this->assertEquals(
            \json_encode(
                \json_decode($expectedJSON, null, 512, JSON_THROW_ON_ERROR),
                JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES
            ),
            (string)$paymentSource
        );
    }
}
