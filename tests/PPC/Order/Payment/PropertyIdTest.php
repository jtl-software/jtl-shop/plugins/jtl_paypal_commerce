<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment;

use PHPUnit\Framework\TestCase;
use Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment\TestSources\PropertyIdPaymentSource;

/**
 * Class PropertyIdTest
 * @package PPC\Order\Payment
 */
class PropertyIdTest extends TestCase
{
    public function testSetGetId(): void
    {
        $paymentSource = new PropertyIdPaymentSource((object)['id' => 'AGFT5374MBH#KI']);
        $this->assertEquals('AGFT5374MBH#KI', $paymentSource->getId());
        $instance = $paymentSource->setId('AGFT5_HJT374MBH#KI');
        $this->assertInstanceOf(PropertyIdPaymentSource::class, $instance);
        $this->assertEquals('AGFT5_HJT374MBH#KI', $instance->getId());
    }
}
