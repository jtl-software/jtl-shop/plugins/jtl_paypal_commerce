<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment;

use JsonException;
use PHPUnit\Framework\TestCase;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\CardDetails;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\GooglePayPaymentSource;

/**
 * Class GooglePayPaymentSourceTest
 * @package PPC\Order\Payment
 */
class GooglePayPaymentSourceTest extends TestCase
{
    private object $data;

    protected function setUp(): void
    {
        parent::setUp();

        $attributes = (object)[
            'verification' => (object)[
                'method'            => 'SCA_ALWAYS',
            ],
        ];
        $this->data = (object)[
            'name'              => 'Tom Tester',
            'email_address'     => 'tom.tester@example.com',
            'phone_number'      => (object)[
                'national_number' => '0049123456789',
            ],
            'card'                => (object)[
                'name' => 'John Doe',
                'type' => 'PREPAID',
                'brand' => 'AMEX',
                'billing_address' => (object)[
                    'address_line_1' => 'Große Ullrichstr. 49',
                    'address_line_2' => 'Eingang über Spiegelstr.',
                    'admin_area_1'   => 'Sachsen-Anhalt',
                    'admin_area_2'   => 'Halle',
                    'postal_code'    => '06108',
                    'country_code'   => 'DE',
                ],
            ],
            'attributes'        => $attributes,
        ];
    }

    public function testInstanceCreation(): void
    {
        $paymentSource = new GooglePayPaymentSource($this->data);
        $this->assertInstanceOf(GooglePayPaymentSource::class, $paymentSource);
    }

    /**
     * @throws JsonException
     */
    public function testJsonSerialize(): void
    {
        $data          = clone $this->data;
        $paymentSource = new GooglePayPaymentSource(clone $data);
        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );

        $paymentSource->setName('Achim van Gröne');
        $data->name = 'Achim van Gröne';
        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );

        $paymentSource->setEmail('tom.tester@test.com');
        $data->email_address = 'tom.tester@test.com';
        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );
    }

    /**
     * @throws JsonException
     */
    public function testSetGetName(): void
    {
        $data          = clone $this->data;
        $paymentSource = new GooglePayPaymentSource(clone $data);
        $this->assertEquals('Tom Tester', $paymentSource->getName());
        $paymentSource->setName('Hans Großmann');
        $this->assertEquals('Hans Großmann', $paymentSource->getName());
        $data->name = 'Hans Großmann';
        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );
    }

    /**
     * @throws JsonException
     */
    public function testSetGetEmail(): void
    {
        $data          = clone $this->data;
        $paymentSource = new GooglePayPaymentSource(clone $data);
        $this->assertEquals('tom.tester@example.com', $paymentSource->getEmail());
        $paymentSource->setEmail('karla.columna@kolumne.com');
        $this->assertEquals('karla.columna@kolumne.com', $paymentSource->getEmail());
        $data->email_address = 'karla.columna@kolumne.com';
        $this->assertEquals('karla.columna@kolumne.com', $paymentSource->fetchPayer()->getEmail());
        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );
    }

    /**
     * @throws JsonException
     */
    public function testSetGetPhone(): void
    {
        $data          = clone $this->data;
        $paymentSource = new GooglePayPaymentSource(clone $data);
        $this->assertEquals('0049123456789', $paymentSource->fetchPayer()->getPhone()->getNumber('00', ''));
        $phone = $paymentSource->fetchPayer()->getPhone();
        $paymentSource->setPhoneNumber($phone->setNumber('004934589765432'));
        $this->assertEquals('004934589765432', $paymentSource->fetchPayer()->getPhone()->getNumber('00', ''));
        $data->phone_number = (object)[
            'national_number' => '004934589765432',
        ];
        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );
    }

    /**
     * @throws JsonException
     */
    public function testSetGetCard(): void
    {
        $data          = clone $this->data;
        $paymentSource = new GooglePayPaymentSource(clone $data);
        $card          = $paymentSource->getCard();
        $this->assertInstanceOf(CardDetails::class, $card);
        $this->assertEquals('John Doe', $card->getName());
        $card = (new CardDetails())
            ->setName('Paul Panzer')
            ->setBrand('VISA')
            ->setType('DEBIT');
        $paymentSource->setCard($card);
        $data->card = (object)[
            'name' => 'Paul Panzer',
            'brand' => 'VISA',
            'type' => 'DEBIT',
        ];
        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );
    }

    public function testSetGetAttributes(): void
    {
        $paymentSource = new GooglePayPaymentSource(clone $this->data);
        $this->assertEquals('SCA_ALWAYS', $paymentSource->getAttribute('verification')->getData()->method);
        $verify = $paymentSource->getAttribute('verification');
        $verify->setData((object)['method' => 'SCA_WHEN_REQUIRED']);
        $paymentSource->addAttribute('verification', $verify);
        $this->assertEquals(
            '{'
                . '"verification":{'
                    . '"method":"SCA_WHEN_REQUIRED"'
                . '}'
            . '}',
            (string)$paymentSource->getAttributes()
        );
    }
}
