<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment\TestSources;

use Plugin\jtl_paypal_commerce\PPC\Order\Payment\AbstractPaymentSource;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\PaymentSourceProperties\PropertyVaultIdTrait;

/**
 * Class PropertyVaultIdPaymentSource
 * @package PPC\Order\Payment
 */
class PropertyVaultIdPaymentSource extends AbstractPaymentSource
{
    use PropertyVaultIdTrait;

    /**
     * @inheritDoc
     */
    public function __construct(?object $data = null)
    {
        parent::__construct($data, ['vault_id']);
    }
}
