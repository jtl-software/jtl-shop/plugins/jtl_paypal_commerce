<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment\TestSources;

use Plugin\jtl_paypal_commerce\PPC\Order\Payment\AbstractPaymentSource;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\PaymentSourceProperties\PropertyStoredCredentialTrait;

/**
 * Class PropertyStoredCredentialPaymentSource
 * @package PPC\Order\Payment
 */
class PropertyStoredCredentialPaymentSource extends AbstractPaymentSource
{
    use PropertyStoredCredentialTrait;

    /**
     * @inheritDoc
     */
    public function __construct(?object $data = null)
    {
        parent::__construct($data, ['stored_credential']);
    }
}
