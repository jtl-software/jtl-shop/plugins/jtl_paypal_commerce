<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment\TestSources;

use DateTime;
use Plugin\jtl_paypal_commerce\PPC\Order\Address;
use Plugin\jtl_paypal_commerce\PPC\Order\ExperienceContext;
use Plugin\jtl_paypal_commerce\PPC\Order\Payer;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\PaymentSourceBuilder;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\PaymentSourceInterface;
use Plugin\jtl_paypal_commerce\PPC\Order\Phone;

/**
 * Class PayPalPaymentSource
 * @package PPC\Order\Payment\TestSources
 */
class PayPalPaymentData
{
    protected string $fundingSource;

    /**
     * PayPalPaymentSource constructor
     */
    public function __construct(string $fundingSource)
    {
        $this->fundingSource = $fundingSource;
    }

    public function createPayer(): Payer
    {
        return (new Payer())
            ->setGivenName('Tom')
            ->setSurname('Tester')
            ->setPhone((new Phone())->setNumber('004956987123'))
            ->setAddress((new Address())
                ->setAddress(['Große Ullrichstr. 49', 'Eingang über Spiegelstr.'])
                ->setCountryCode('DE')
                ->setPostalCode('06108')
                ->setCity('Halle'))
            ->setEmail('tom.tester@example.com')
            ->setBirthDate(new DateTime('1990-01-01'));
    }

    public function createExperienceContext(
        PaymentSourceInterface $paymentSource,
        string $locale,
        string $shippingContext,
        string $payerAction
    ): ExperienceContext {
        return $paymentSource->buildExperienceContext()
            ->setLocale($locale)
            ->setBrandName('Mein schöner Shop')
            ->setShippingPreference($shippingContext)
            ->setUserAction($payerAction)
            ->setCustomerServiceInstruction('Unsere Telefonnummer finden Sie auf unserer Webseite');
    }

    public function build(): PaymentSourceInterface
    {
        $paymentSource = (new PaymentSourceBuilder($this->fundingSource))->build();
        $paymentSource->applyPayer($this->createPayer())
                      ->setExperienceContext($this->createExperienceContext(
                          $paymentSource,
                          'de-DE',
                          ExperienceContext::SHIPPING_PROVIDED,
                          ExperienceContext::USER_ACTION_PAY_NOW
                      ));

        return $paymentSource;
    }
}
