<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment\TestSources;

use Plugin\jtl_paypal_commerce\PPC\Order\ExperienceContext;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\AbstractPaymentSource;

/**
 * Class TestPaymentSource
 * @package ${NAMESPACE}
 */
class TestPaymentSource extends AbstractPaymentSource
{
    public function __construct(?object $data = null)
    {
        parent::__construct($data, [
            'my_name',
            'email',
            'experience_context',
        ]);
    }

    protected function mapEntitie(string $name): string
    {
        return match ($name) {
            'name'          => 'my_name',
            'email_address' => 'email',
            default => $name,
        };
    }

    public function serializeMyName(object $data): void
    {
        $data->my_name = 'MY:' . $data->my_name;
    }

    public function initdataExperienceContext(object|array|string $data): void
    {
        if (!($data instanceof ExperienceContext)) {
            $this->data->experience_context = new ExperienceContext($data);
        }
    }
}
