<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment\TestSources;

use Plugin\jtl_paypal_commerce\PPC\Order\Payment\AbstractPaymentSource;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\PaymentSourceProperties\PropertyBankReferenceTrait;

/**
 * Class PropertyBankReferencePaymentSource
 * @package PPC\Order\Payment\TestSources
 */
class PropertyBankReferencePaymentSource extends AbstractPaymentSource
{
    use PropertyBankReferenceTrait;

    /**
     * @inheritDoc
     */
    public function __construct(?object $data = null)
    {
        parent::__construct($data, ['payment_reference', 'deposit_bank_details']);
    }
}
