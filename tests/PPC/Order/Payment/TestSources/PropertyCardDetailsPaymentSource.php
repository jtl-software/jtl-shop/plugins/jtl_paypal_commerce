<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment\TestSources;

use Plugin\jtl_paypal_commerce\PPC\Order\Payment\AbstractPaymentSource;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\PaymentSourceProperties\PropertyCardDetailsTrait;

/**
 * Class PropertyCardDetailsPaymentSource
 * @package PPC\Order\Payment\TestSources
 */
class PropertyCardDetailsPaymentSource extends AbstractPaymentSource
{
    use PropertyCardDetailsTrait;

    /**
     * @inheritDoc
     */
    public function __construct(?object $data = null)
    {
        parent::__construct($data, [
            'card',
        ]);
    }
}
