<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment\TestSources;

use Plugin\jtl_paypal_commerce\PPC\Order\Payment\PaymentSourceBuilder;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\PaymentSourceInterface;

/**
 * Class ApplePaymentData
 * @package PPC\Order\Payment\TestSources
 */
class ApplePaymentData extends PayPalPaymentData
{
    public function build(): PaymentSourceInterface
    {
        $paymentSource = (new PaymentSourceBuilder($this->fundingSource))->build();
        $paymentSource->applyPayer($this->createPayer());

        return $paymentSource;
    }
}
