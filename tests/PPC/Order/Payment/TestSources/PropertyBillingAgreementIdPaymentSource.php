<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment\TestSources;

use Plugin\jtl_paypal_commerce\PPC\Order\Payment\AbstractPaymentSource;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\PaymentSourceProperties\PropertyBillingAgreementIdTrait;

/**
 * Class PropertyBillingAgreementIdPaymentSource
 * @package PPC\Order\Payment
 */
class PropertyBillingAgreementIdPaymentSource extends AbstractPaymentSource
{
    use PropertyBillingAgreementIdTrait;

    /**
     * @inheritDoc
     */
    public function __construct(?object $data = null)
    {
        parent::__construct($data, [
            'billing_agreement_id',
        ]);
    }
}
