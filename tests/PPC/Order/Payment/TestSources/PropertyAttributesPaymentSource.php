<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment\TestSources;

use Plugin\jtl_paypal_commerce\PPC\Order\Payment\AbstractPaymentSource;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\PaymentSourceProperties\PropertyAttributesTrait;

/**
 * Class PropertyAttributesPaymentSource
 * @package PPC\Order\Payment
 */
class PropertyAttributesPaymentSource extends AbstractPaymentSource
{
    use PropertyAttributesTrait;

    /**
     * @inheritDoc
     */
    public function __construct(?object $data = null)
    {
        parent::__construct($data, ['attributes']);
    }
}
