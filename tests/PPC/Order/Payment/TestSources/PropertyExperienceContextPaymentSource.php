<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment\TestSources;

use Plugin\jtl_paypal_commerce\PPC\Order\Payment\AbstractPaymentSource;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\PaymentSourceProperties\PropertyExperienceContextTrait;

/**
 * Class PropertyExperienceContextPaymentSource
 * @package PPC\Order\Payment
 */
class PropertyExperienceContextPaymentSource extends AbstractPaymentSource
{
    use PropertyExperienceContextTrait;

    /**
     * @inheritDoc
     */
    public function __construct(?object $data = null)
    {
        parent::__construct($data, ['experience_context']);
    }
}
