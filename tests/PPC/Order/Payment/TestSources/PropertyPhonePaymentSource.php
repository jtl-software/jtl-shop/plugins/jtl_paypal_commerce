<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment\TestSources;

use Plugin\jtl_paypal_commerce\PPC\Order\Payment\AbstractPaymentSource;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\PaymentSourceProperties\PropertyPhoneNumberTrait;

/**
 * Class PropertyPhonePaymentSource
 * @package PPC\Order\Payment\TestSources
 */
class PropertyPhonePaymentSource extends AbstractPaymentSource
{
    use PropertyPhoneNumberTrait;

    /**
     * @inheritDoc
     */
    public function __construct(?object $data = null)
    {
        parent::__construct($data, [
            'phone_number',
        ]);
    }
}
