<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment\TestSources;

use Plugin\jtl_paypal_commerce\PPC\Order\Payment\AbstractPaymentSource;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\PaymentSourceProperties\PropertyBillingAdressTrait;

/**
 * Class PropertyBillingAdressPaymentSource
 * @package Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment
 */
class PropertyBillingAdressPaymentSource extends AbstractPaymentSource
{
    use PropertyBillingAdressTrait;

    /**
     * @inheritDoc
     */
    public function __construct(?object $data = null)
    {
        parent::__construct($data, ['billing_address']);
    }
}
