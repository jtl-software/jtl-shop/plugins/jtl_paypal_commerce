<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment\TestSources;

use Plugin\jtl_paypal_commerce\PPC\Order\Payment\AbstractPaymentSource;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\PaymentSourceProperties\PropertyBillingAdressTrait;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\PaymentSourceProperties\PropertyBirthDateTrait;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\PaymentSourceProperties\PropertyEmailAddressTrait;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\PaymentSourceProperties\PropertyNameTrait;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\PaymentSourceProperties\PropertyPhoneNumberTrait;

/**
 * Class TestPayerPaymentSource
 * @package PPC\Order\Payment\TestSources
 */
class TestPayerPaymentSource extends AbstractPaymentSource
{
    use PropertyNameTrait;
    use PropertyEmailAddressTrait;
    use PropertyBirthDateTrait;
    use PropertyPhoneNumberTrait;
    use PropertyBillingAdressTrait;

    public function __construct(?object $data = null)
    {
        parent::__construct($data, [
            'name',
            'email_address',
            'birth_date',
            'phone_number',
            'address'
        ]);
    }

    protected function mapEntitie(string $name): string
    {
        return match ($name) {
            'billing_address' => 'address',
            default => parent::mapEntitie($name),
        };
    }
}
