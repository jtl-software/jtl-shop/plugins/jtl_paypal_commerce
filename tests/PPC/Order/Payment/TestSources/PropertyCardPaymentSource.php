<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment\TestSources;

use Plugin\jtl_paypal_commerce\PPC\Order\Payment\AbstractPaymentSource;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\PaymentSourceProperties\PropertyCardTrait;

/**
 * Class PropertyCardPaymentSource
 * @package PPC\Order\Payment
 */
class PropertyCardPaymentSource extends AbstractPaymentSource
{
    use PropertyCardTrait;

    /**
     * @inheritDoc
     */
    public function __construct(?object $data = null)
    {
        parent::__construct($data, [
            'number',
            'security_code',
            'expiry',
            ]);
    }
}
