<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment;

use PHPUnit\Framework\TestCase;
use Plugin\jtl_paypal_commerce\PPC\Order\Address;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\CardDetails;
use Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment\TestSources\PropertyCardDetailsPaymentSource;

/**
 * Class PropertyCardDetailsTest
 * @package PPC\Order\Payment
 */
class PropertyCardDetailsTest extends TestCase
{
    /**
     * @throws \JsonException
     */
    public function testSetGetCard(): void
    {
        $data          = (object)[
            'card' => (object)[
                'name'            => 'John Doe',
                'type'            => 'PREPAID',
                'brand'           => 'AMEX',
                'billing_address' => (object)[
                    'address_line_1' => 'Große Ullrichstr. 49',
                    'address_line_2' => 'Eingang über Spiegelstr.',
                    'admin_area_1'   => 'Sachsen-Anhalt',
                    'admin_area_2'   => 'Halle',
                    'postal_code'    => '06108',
                    'country_code'   => 'DE',
                ],
            ]];
        $paymentSource = new PropertyCardDetailsPaymentSource(clone $data);

        $card = $paymentSource->getCard();
        $this->assertInstanceOf(CardDetails::class, $card);
        $this->assertEquals('John Doe', $card->getName());
        $this->assertEquals('PREPAID', $card->getType());
        $this->assertEquals('AMEX', $card->getBrand());

        $address = $card->getBillingAddress();
        $this->assertInstanceOf(Address::class, $address);
        $this->assertEquals('Große Ullrichstr. 49', $address->getAddress()[0]);
        $address = new Address();
        $address->setAddress(['Kleine Ullrichstr. 87']);
        $card->setBillingAddress($address);
        $this->assertEquals('Kleine Ullrichstr. 87', $card->getBillingAddress()->getAddress()[0]);
        $this->assertEquals('MASTERCARD', $card->setBrand('MASTERCARD')->getBrand());
        $this->assertEquals('CREDIT', $card->setType('CREDIT')->getType());
        $this->assertEquals('Tom Tester', $card->setName('Tom Tester')->getName());

        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );
    }
}
