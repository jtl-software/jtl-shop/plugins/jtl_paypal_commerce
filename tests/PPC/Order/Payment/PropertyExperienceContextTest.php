<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment;

use PHPUnit\Framework\TestCase;
use Plugin\jtl_paypal_commerce\PPC\Order\ExperienceContext;
use Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment\TestSources\PropertyExperienceContextPaymentSource;

class PropertyExperienceContextTest extends TestCase
{
    public function testSetGetExperienceContext(): void
    {
        $paymentSource = new PropertyExperienceContextPaymentSource();
        $this->assertNull($paymentSource->getExperienceContext());
        $experienceContext = new ExperienceContext();
        $instance          = $paymentSource->setExperienceContext($experienceContext);
        $this->assertInstanceOf(PropertyExperienceContextPaymentSource::class, $instance);
        $this->assertInstanceOf(ExperienceContext::class, $instance->getExperienceContext());
        $this->assertEquals($experienceContext, $instance->getExperienceContext());
    }
}
