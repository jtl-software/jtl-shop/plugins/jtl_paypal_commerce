<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment;

use PHPUnit\Framework\TestCase;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\StoredCredential;
use Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment\TestSources\PropertyStoredCredentialPaymentSource;

class PropertyStoredCredentialTest extends TestCase
{
    public function testSetGetStoredCredential(): void
    {
        $paymentSource = new PropertyStoredCredentialPaymentSource(
            (object)['stored_credential' => (object)[
                'payment_initiator' => 'MERCHANT',
                'payment_type'      => 'RECURRING',
                'usage'             => 'SUBSEQUENT',
            ]]
        );
        $credentials   = $paymentSource->getStoredCredential();
        $this->assertInstanceOf(StoredCredential::class, $credentials);
        $this->assertEquals(StoredCredential::USAGE_SUBSEQUENT, $credentials->getUsage());

        $credentials = new storedCredential((object)[
            'payment_initiator' => 'MERCHANT',
            'payment_type'      => 'RECURRING',
            'usage'             => 'FIRST',
        ]);
        $paymentSource->setStoredCredential($credentials);
        $this->assertEquals(
            StoredCredential::USAGE_FIRST,
            $paymentSource->getStoredCredential()->getUsage()
        );

        $paymentSource->setStoredCredential();
        $this->assertNull($paymentSource->getStoredCredential());
    }
}
