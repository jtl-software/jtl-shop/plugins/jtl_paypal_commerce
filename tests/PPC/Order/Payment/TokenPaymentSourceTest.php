<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment;

use JsonException;
use PHPUnit\Framework\TestCase;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\PaymentSourceProperties\PropertyConstants;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\TokenPaymentSource;

/**
 * Class TokenPaymentSourceTest
 * @package PPC\Order\Payment
 */
class TokenPaymentSourceTest extends TestCase
{
    private object $data;

    protected function setUp(): void
    {
        $this->data = (object)[
            'id'   => 'ACFGH-jk:23',
            'type' => PropertyConstants::TYPE_BILLING_AGREEMENT
        ];
    }

    public function testInstanceCreation(): void
    {
        $paymentSource = new TokenPaymentSource($this->data);
        $this->assertInstanceOf(TokenPaymentSource::class, $paymentSource);
    }

    /**
     * @throws JsonException
     */
    public function testJsonSerialize(): void
    {
        $data          = clone $this->data;
        $paymentSource = new TokenPaymentSource(clone $data);
        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );

        $paymentSource->setId('ACFGH-jk:25');
        $data->id = 'ACFGH-jk:25';
        $this->assertEquals(
            \json_encode($data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );
    }

    /**
     * @throws JsonException
     */
    public function testInvalidJsonSerialize(): void
    {
        $data                = clone $this->data;
        $data->email_address = 'test@example.com';
        $paymentSource       = new TokenPaymentSource($data);
        $this->assertEquals(
            \json_encode($this->data, JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
            (string)$paymentSource
        );
    }
}
