<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment;

use PHPUnit\Framework\TestCase;
use Plugin\jtl_paypal_commerce\PPC\Request\Serializer\JSON;
use Plugin\jtl_paypal_commerce\tests\PPC\Order\Payment\TestSources\PropertyAttributesPaymentSource;

class PropertyAttributesTest extends TestCase
{
    public function testSetAttributes(): void
    {
        $paymentSource = new PropertyAttributesPaymentSource(
            (object)['attributes' => (object)[
                'customer'     => (object)[
                    'id'            => '123',
                    'email_address' => 'test@example.com',
                ],
                'verification' => 'SCA_ALWAYS',
            ]]
        );
        $this->assertEquals('SCA_ALWAYS', $paymentSource->getAttribute('verification')->getData());
        $this->assertEquals('123', $paymentSource->getAttribute('customer')->getData()->id);
        $paymentSource->addAttribute('vault', new JSON((object)[
            'store_in_vault' => 'ON_SUCCESS',
        ]));
        $this->assertEquals('ON_SUCCESS', $paymentSource->getAttribute('vault')->getData()->store_in_vault);
    }

    public function testSerializeAttributes(): void
    {
        $paymentSource = new PropertyAttributesPaymentSource(
            (object)['attributes' => (object)[
                'customer'     => (object)[
                    'id'            => '123',
                    'email_address' => 'test@example.com',
                ],
                'verification' => 'SCA_ALWAYS',
            ]]
        );
        $paymentSource->addAttribute('vault', new JSON((object)[
            'store_in_vault' => 'ON_SUCCESS',
        ]));
        $this->assertEquals(
            '{"attributes":{'
                . '"customer":{'
                    . '"id":"123",'
                    . '"email_address":"test@example.com"'
                . '},'
                . '"verification":"SCA_ALWAYS",'
                . '"vault":{'
                    . '"store_in_vault":"ON_SUCCESS"'
                . '}'
            . '}}',
            (string)$paymentSource
        );
    }
}
