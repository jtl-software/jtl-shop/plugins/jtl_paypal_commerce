<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order;

use JsonException;
use Plugin\jtl_paypal_commerce\PPC\Order\Order;
use PHPUnit\Framework\TestCase;
use Plugin\jtl_paypal_commerce\PPC\Order\Payer;
use Plugin\jtl_paypal_commerce\PPC\Order\Payment\PayPalPaymentSource;
use Plugin\jtl_paypal_commerce\PPC\Order\Purchase\PurchaseUnit;

class OrderTest extends TestCase
{
    private object $data;

    /**
     * @throws JsonException
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->data = \json_decode(
            '{
                "id": "5O190127TN364715T",
                "status": "APPROVED",
                "intent": "CAPTURE",
                "payment_source": {
                    "paypal": {
                        "name": {
                        "given_name": "John",
                        "surname": "Doe"
                        },
                        "email_address": "customer@example.com",
                        "account_id": "QYR5Z8XDVJNXQ"
                    }
                },
                "purchase_units": [
                    {
                        "reference_id": "d9f80740-38f0-11e8-b467-0ed5f89f718b",
                        "amount": {
                            "currency_code": "USD",
                            "value": "100.00"
                        }
                    }
                ],
                "payer": {
                    "name": {
                        "given_name": "Toni",
                        "surname": "Payer"
                    },
                    "email_address": "toni.payer@example.com",
                    "payer_id": "XDVJNXQQYR5Z8"
                },
                "create_time": "2018-04-01T21:18:49Z",
                "links": [
                    {
                        "href": "https://api-m.paypal.com/v2/checkout/orders/5O190127TN364715T",
                        "rel": "self",
                        "method": "GET"
                    },
                    {
                        "href": "https://www.paypal.com/checkoutnow?token=5O190127TN364715T",
                        "rel": "approve",
                        "method": "GET"
                    },
                    {
                        "href": "https://api-m.paypal.com/v2/checkout/orders/5O190127TN364715T",
                        "rel": "update",
                        "method": "PATCH"
                    },
                    {
                        "href": "https://api-m.paypal.com/v2/checkout/orders/5O190127TN364715T/capture",
                        "rel": "capture",
                        "method": "POST"
                    }
                ]
            }',
            false,
            512,
            \JSON_THROW_ON_ERROR
        );
    }

    public function testGetId(): void
    {
        $order = new Order(clone $this->data);
        $this->assertEquals('5O190127TN364715T', $order->getId());
    }

    public function testGetStatus(): void
    {
        $order = new Order(clone $this->data);
        $this->assertEquals('APPROVED', $order->getStatus());
    }

    public function testGetIntent(): void
    {
        $order = new Order(clone $this->data);
        $this->assertEquals('CAPTURE', $order->getIntent());
    }

    public function testGetPaymentSources(): void
    {
        $order = new Order(clone $this->data);
        $this->assertCount(1, $order->getPaymentSources());
        $this->assertContainsEquals('paypal', $order->getPaymentSources());
    }

    public function testGetPaymentSource(): void
    {
        $order         = new Order(clone $this->data);
        $paymentSource = $order->getPaymentSource();
        $this->assertInstanceOf(PayPalPaymentSource::class, $paymentSource);
        $this->assertEquals('John Doe', $paymentSource->getName());
        $this->assertEquals('customer@example.com', $paymentSource->getEmail());
        $payer = $paymentSource->fetchPayer();
        $this->assertEquals('John', $payer->getGivenName());
        $this->assertEquals('Doe', $payer->getSurname());
        $this->assertEquals('customer@example.com', $payer->getEmail());
        $this->assertEquals('QYR5Z8XDVJNXQ', $payer->getPayerid());
    }

    public function testGetPayer(): void
    {
        $order = new Order(clone $this->data);
        $payer = $order->getPayer();
        $this->assertInstanceOf(Payer::class, $payer);
        $this->assertEquals('Toni', $payer->getGivenName());
        $this->assertEquals('Payer', $payer->getSurname());
        $this->assertEquals('toni.payer@example.com', $payer->getEmail());
        $this->assertEquals('XDVJNXQQYR5Z8', $payer->getPayerid());

        $data = clone $this->data;
        unset($data->payer);
        $order = new Order($data);
        $payer = $order->getPayer();
        $this->assertInstanceOf(Payer::class, $payer);
        $this->assertEquals('John', $payer->getGivenName());
        $this->assertEquals('Doe', $payer->getSurname());
        $this->assertEquals('customer@example.com', $payer->getEmail());
        $this->assertEquals('QYR5Z8XDVJNXQ', $payer->getPayerid());

        unset($data->payment_source);
        $this->assertNull($order->getPayer());
    }

    public function testGetPurchase(): void
    {
        $order = new Order(clone $this->data);
        $this->assertEquals(PurchaseUnit::REFERENCE_DEFAULT, $order->getPurchase()->getReferenceId());
        $purchase = $order->getPurchase('d9f80740-38f0-11e8-b467-0ed5f89f718b');
        $this->assertEquals('100.00', $purchase->getAmount()->getValue());
        $this->assertEquals('USD', $purchase->getAmount()->getCurrencyCode());
    }

    public function testGetLink(): void
    {
        $order = new Order(clone $this->data);
        $this->assertEquals('https://api-m.paypal.com/v2/checkout/orders/5O190127TN364715T', $order->getLink('self'));
    }

    public function testGetLinkObject(): void
    {
        $order   = new Order(clone $this->data);
        $linkObj = $order->getLinkObject('capture');
        $this->assertEquals('https://api-m.paypal.com/v2/checkout/orders/5O190127TN364715T/capture', $linkObj->href);
        $this->assertEquals('POST', $linkObj->method);
    }
}
