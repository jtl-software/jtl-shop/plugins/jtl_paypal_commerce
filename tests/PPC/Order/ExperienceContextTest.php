<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC\Order;

use InvalidArgumentException;
use JsonException;
use PHPUnit\Framework\TestCase;
use Plugin\jtl_paypal_commerce\PPC\Order\ExperienceContext;
use stdClass;

class ExperienceContextTest extends TestCase
{
    private string $data;

    /**
     * @throws JsonException
     */
    protected function setUp(): void
    {
        parent::setUp();

        $data                                = new stdClass();
        $data->brand_name                    = 'Mein schöner Shop';
        $data->shipping_preference           = ExperienceContext::SHIPPING_NO_SHIPPING;
        $data->landing_page                  = ExperienceContext::PAGE_GUEST_CHECKOUT;
        $data->user_action                   = ExperienceContext::USER_ACTION_CONTINUE;
        $data->payment_method_preference     = ExperienceContext::METHOD_UNRESTRICTED;
        $data->locale                        = 'de-DE';
        $data->return_url                    = 'https://example.com/return';
        $data->cancel_url                    = 'https://example.com/cancel';
        $data->customer_service_instructions = ['Handle with care.'];

        $this->data = \json_encode($data, \JSON_THROW_ON_ERROR | \JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }

    /**
     * @throws JsonException
     */
    public function testInstanceCreation(): ExperienceContext
    {
        $experienceContext = new ExperienceContext(\json_decode($this->data, false, 512, \JSON_THROW_ON_ERROR));
        $this->assertInstanceOf(ExperienceContext::class, $experienceContext);

        return $experienceContext;
    }

    /**
     * @depends testInstanceCreation
     * @param ExperienceContext $experienceContext
     * @return void
     */
    public function testGetSetLocale(ExperienceContext $experienceContext): void
    {
        $this->assertEquals('de-DE', $experienceContext->getLocale());
        $experienceContext->setLocale('en-EN');
        $this->assertEquals('en-EN', $experienceContext->getLocale());

        $this->expectException(InvalidArgumentException::class);
        $experienceContext->setLocale('DE-EN');
    }

    /**
     * @depends testInstanceCreation
     * @param ExperienceContext $experienceContext
     * @return void
     */
    public function testGetSetBrandName(ExperienceContext $experienceContext): void
    {
        $this->assertEquals('Mein schöner Shop', $experienceContext->getBrandName());
        $experienceContext->setBrandName('Mein sehr schöner Shop');
        $this->assertEquals('Mein sehr schöner Shop', $experienceContext->getBrandName());
        $experienceContext->setBrandName();
        $this->assertEquals('', $experienceContext->getBrandName());
    }

    /**
     * @depends testInstanceCreation
     * @param ExperienceContext $experienceContext
     * @return void
     */
    public function testGetSetUserAction(ExperienceContext $experienceContext): void
    {
        $this->assertEquals(ExperienceContext::USER_ACTION_CONTINUE, $experienceContext->getUserAction());
        $experienceContext->setUserAction(ExperienceContext::USER_ACTION_PAY_NOW);
        $this->assertEquals(ExperienceContext::USER_ACTION_PAY_NOW, $experienceContext->getUserAction());
        $experienceContext->setUserAction();
        $this->assertEquals(ExperienceContext::USER_ACTION_CONTINUE, $experienceContext->getUserAction());

        $this->expectException(InvalidArgumentException::class);
        $experienceContext->setUserAction('UNKNOW');
    }

    /**
     * @depends testInstanceCreation
     * @param ExperienceContext $experienceContext
     * @return void
     */
    public function testGetSetLandingPage(ExperienceContext $experienceContext): void
    {
        $this->assertEquals(ExperienceContext::PAGE_GUEST_CHECKOUT, $experienceContext->getLandingPage());
        $experienceContext->setLandingPage(ExperienceContext::PAGE_LOGIN);
        $this->assertEquals(ExperienceContext::PAGE_LOGIN, $experienceContext->getLandingPage());
        $experienceContext->setLandingPage();
        $this->assertEquals(ExperienceContext::PAGE_NO_PREFERENCE, $experienceContext->getLandingPage());

        $this->expectException(InvalidArgumentException::class);
        $experienceContext->setLandingPage('ERROR');
    }

    /**
     * @depends testInstanceCreation
     * @param ExperienceContext $experienceContext
     * @return void
     */
    public function testGetSetPaymentMethodPreference(ExperienceContext $experienceContext): void
    {
        $this->assertEquals(ExperienceContext::METHOD_UNRESTRICTED, $experienceContext->getPaymentMethodPreference());
        $experienceContext->setPaymentMethodPreference(ExperienceContext::METHOD_IMMEDIATE_PAYMENT_REQUIRED);
        $this->assertEquals(
            ExperienceContext::METHOD_IMMEDIATE_PAYMENT_REQUIRED,
            $experienceContext->getPaymentMethodPreference()
        );
        $experienceContext->setPaymentMethodPreference();
        $this->assertEquals(ExperienceContext::METHOD_UNRESTRICTED, $experienceContext->getPaymentMethodPreference());

        $this->expectException(InvalidArgumentException::class);
        $experienceContext->setPaymentMethodPreference('NO_METHOD');
    }

    /**
     * @depends testInstanceCreation
     * @param ExperienceContext $experienceContext
     * @return void
     */
    public function testGetSetShippingPreference(ExperienceContext $experienceContext): void
    {
        $this->assertEquals(ExperienceContext::SHIPPING_NO_SHIPPING, $experienceContext->getShippingPreference());
        $experienceContext->setShippingPreference(ExperienceContext::SHIPPING_PROVIDED);
        $this->assertEquals(ExperienceContext::SHIPPING_PROVIDED, $experienceContext->getShippingPreference());
        $experienceContext->setShippingPreference();
        $this->assertEquals(ExperienceContext::SHIPPING_FROM_FILE, $experienceContext->getShippingPreference());

        $this->expectException(InvalidArgumentException::class);
        $experienceContext->setShippingPreference('SHIPPING_FAILURE');
    }

    /**
     * @depends testInstanceCreation
     * @param ExperienceContext $experienceContext
     * @return void
     */
    public function testGetSetCancelUrl(ExperienceContext $experienceContext): void
    {
        $this->assertEquals('https://example.com/cancel', $experienceContext->getCancelUrl());
        $experienceContext->setCancelURL();
        $this->assertEquals('', $experienceContext->getCancelUrl());
        $experienceContext->setCancelUrl('https://example.com/cancelnow');
        $this->assertEquals('https://example.com/cancelnow', $experienceContext->getCancelUrl());
    }

    /**
     * @depends testInstanceCreation
     * @param ExperienceContext $experienceContext
     * @return void
     */
    public function testGetSetReturnUrl(ExperienceContext $experienceContext): void
    {
        $this->assertEquals('https://example.com/return', $experienceContext->getReturnUrl());
        $experienceContext->setReturnURL();
        $this->assertEquals('', $experienceContext->getReturnUrl());
        $experienceContext->setReturnURL('https://example.com/returnnow');
        $this->assertEquals('https://example.com/returnnow', $experienceContext->getReturnUrl());
    }

    /**
     * @depends testInstanceCreation
     * @param ExperienceContext $experienceContext
     * @return void
     */
    public function testGetSetCustomerServiceInstruction(ExperienceContext $experienceContext): void
    {
        $this->assertEquals('Handle with care.', $experienceContext->getCustomerServiceInstruction());
        $experienceContext->setCustomerServiceInstruction('Dont get up');
        $this->assertEquals('Dont get up', $experienceContext->getCustomerServiceInstruction());
    }

    /**
     * @throws JsonException
     */
    public function testIsPropertyActive(): void
    {
        $experienceContext = new ExperienceContext(
            \json_decode($this->data, false, 512, JSON_THROW_ON_ERROR),
            ['brand_name', 'locale']
        );
        $this->assertTrue($experienceContext->isPropertyActive('brand_name'));
        $this->assertTrue($experienceContext->isPropertyActive('locale'));
        $this->assertFalse($experienceContext->isPropertyActive('user_action'));
        $this->assertEquals(
            '{'
            . '"brand_name":"Mein schöner Shop",'
            . '"locale":"de-DE"'
            . '}',
            (string)$experienceContext
        );
    }

    /**
     * @throws JsonException
     */
    public function testJsonSerialize(): void
    {
        $experienceContext = new ExperienceContext(\json_decode($this->data, false, 512, JSON_THROW_ON_ERROR));
        $this->assertEquals($this->data, (string)$experienceContext);
    }
}
