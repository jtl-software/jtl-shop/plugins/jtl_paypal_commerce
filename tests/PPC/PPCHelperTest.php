<?php

declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\tests\PPC;

use InvalidArgumentException;
use Plugin\jtl_paypal_commerce\PPC\PPCHelper;
use PHPUnit\Framework\TestCase;

class PPCHelperTest extends TestCase
{
    public function testShortenStr(): void
    {
        $this->assertEquals('Dieser Text', PPCHelper::shortenStr('Dieser Text muss gekürtzt werden!', 11, ''));
        $this->assertEquals('Dieser T...', PPCHelper::shortenStr('Dieser Text muss gekürtzt werden!', 11));
        $this->assertEquals('Dieser ...!', PPCHelper::shortenStr('Dieser Text muss gekürtzt werden!', 11, '...!'));
        $this->assertEquals(
            'Dieser Text muss nicht gekürtzt werden!',
            PPCHelper::shortenStr('Dieser Text muss nicht gekürtzt werden!', 39, '')
        );
        $this->assertEquals(
            'Und dieser erst recht nicht',
            PPCHelper::shortenStr('Und dieser erst recht nicht', 255, '')
        );
    }

    public function testValidateStr(): void
    {
        $xCount = 0;
        $this->assertEquals('123456789', PPCHelper::validateStr('123456789', 1, 9));
        $this->assertEquals('12345678', PPCHelper::validateStr('12345678', 8, 10));
        $this->assertEquals('AMEX', PPCHelper::validateStr('AMEX', 4, 4, '^[A-Z_]+$'));
        try {
            PPCHelper::validateStr('12345678', 9, 10);
        } catch (InvalidArgumentException $e) {
            $this->assertEquals('Value must have [ 9..10 ] characters', $e->getMessage());
            $xCount++;
        }
        try {
            PPCHelper::validateStr('12345678', 9, 10);
        } catch (InvalidArgumentException $e) {
            $this->assertEquals('Value must have [ 9..10 ] characters', $e->getMessage());
            $xCount++;
        }
        try {
            PPCHelper::validateStr('amex', 4, 4, '^[A-Z_]+$');
        } catch (InvalidArgumentException $e) {
            $this->assertEquals('Value does not comply with the rule', $e->getMessage());
            $xCount++;
        }
        $this->assertEquals(3, $xCount);
    }
}
